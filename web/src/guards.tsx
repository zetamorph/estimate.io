import { Alert, Intent } from '@blueprintjs/core';
import { MUTATIONS } from '@flyacts/estimate-ts-shared';
import React from 'react';
import ReactDOM from 'react-dom';
import { client } from './client';

export interface LeaveSessionGuardData {
    sessionId: string;
    participantId: string;
    type: '@@LeaveSession';
}

export type GuardData = { message: string } & LeaveSessionGuardData;

export const routeGuard = (message: string, cb: (ok: boolean) => void) => {
    const container = document.createElement('div');
    document.body.appendChild(container);
    const data: GuardData = JSON.parse(message);

    const handleClose = async (shouldLeave: boolean) => {
        ReactDOM.unmountComponentAtNode(container);
        if (shouldLeave) {
            switch (data.type) {
                case '@@LeaveSession': {
                    const { sessionId, participantId } = data;
                    await client.mutate({
                        mutation: MUTATIONS.LEAVE_SESSION,
                        variables: {
                            participantId,
                            sessionId,
                        },
                    });
                    /* Clear all local data */
                    await client.resetStore();
                }
            }
            return cb(true);
        }
        return cb(false);
    };

    ReactDOM.render(
        <Alert
            isOpen={true}
            onClose={handleClose}
            cancelButtonText="Cancel"
            intent={Intent.WARNING}
        >{data.message}</Alert>,
        container,
    );
};
