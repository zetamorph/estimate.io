import { FocusStyleManager } from '@blueprintjs/core';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/datetime/lib/css/blueprint-datetime.css';
import React from 'react';
import { ApolloProvider } from 'react-apollo';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { App } from './App';
import { client } from './client';
import { routeGuard } from './guards';
import './index.css';
import * as serviceWorker from './serviceWorker';

FocusStyleManager.onlyShowFocusOnTabs();

ReactDOM.render(<>
        <ApolloProvider client={client}>
            <BrowserRouter getUserConfirmation={routeGuard}>
                <App />
            </BrowserRouter>
        </ApolloProvider>
    </>,
    document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
