import { AnchorButton, Button, ButtonGroup, FormGroup, NumericInput, Popover, Tooltip } from '@blueprintjs/core';
import { MUTATIONS, QUERIES } from '@flyacts/estimate-ts-shared';
import { Task, TaskState } from '@flyacts/estimate-ts-shared';
import React, { useState } from 'react';
import { adopt } from 'react-adopt';
import { Mutation, Query } from 'react-apollo';
import { logger } from '../../logger';
import { averageEstimates } from '../../util';
import { Loading } from '../Loading';
import styles from './CompleteTask.module.css';
// tslint:disable:no-shadowed-variable

interface CompleteTaskProps {
    task: Task;
}

interface RenderProps {
    render: any;
    task: Task;
}

const getEstimations = ({ render, task }: RenderProps) => (
    <Query
        query={QUERIES.GET_ESTIMATES}
        variables={{ taskId: task.id }}
    >{render}</Query>
);

const completeTask = ({ render }: RenderProps) => (
    <Mutation
        mutation={MUTATIONS.COMPLETE_TASK}
    >{render}</Mutation>
);

const CompleteTaskWithEstimations = adopt({
    completeTask,
    getEstimations,
});

const WithGraphQL = ({ task, hidePopover }: any) => (
    <CompleteTaskWithEstimations
        task={task}
    >{({ completeTask, getEstimations }: any) => {
        if (getEstimations.loading) {
            return <Loading />;
        }

        const averageEstimate = averageEstimates(getEstimations.data.estimationServiceGetEstimates.estimates);
        return (
            <FinalEstimationForm
                completeTask={completeTask}
                averageEstimate={averageEstimate}
                task={task}
                hidePopover={hidePopover}
            />
        );
    }}</CompleteTaskWithEstimations>
);

const FinalEstimationForm = ({ completeTask, averageEstimate, hidePopover, task }: any) => {
    const [finalEstimation, setFinalEstimation] = useState<number>(averageEstimate);

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        completeTask({
            variables: {
                estimation: finalEstimation,
                taskId: task.id,
            },
        })
        .then(() => hidePopover())
        .catch(logger.error);
    };

    return (
        <div className={styles.popoverContent}>
            <form onSubmit={handleSubmit}>
                <FormGroup
                    className={styles.formGroup}
                    label="Final Estimation"
                    labelInfo="(required)"
                >
                    <NumericInput
                        fill={true}
                        value={finalEstimation}
                        onValueChange={setFinalEstimation}
                        min={0}
                        max={255}
                        buttonPosition="left"
                    ></NumericInput>
                    <ButtonGroup
                        className={styles.buttonGroup}
                        fill={true}
                    >
                        <Button
                            type="submit"
                            intent="success"
                        >Save</Button>
                        <Button
                            intent="danger"
                            type="button"
                            onClick={() => hidePopover()}
                        >Cancel</Button>
                    </ButtonGroup>
                </FormGroup>
            </form>
        </div>
    );
};

export const CompleteTask = ({ task }: CompleteTaskProps) => {
    const [showPopover, setShowPopover] = useState(false);
    const canComplete = task.state !== TaskState.COMPLETE;
    const toolTipContent = canComplete
        ? 'Set a final estimation for this task'
        : 'This task is already complete';

    const completeTaskButton = (
        <Tooltip content={toolTipContent}>
            <AnchorButton
                icon="tick"
                intent="success"
                onClick={() => setShowPopover(true)}
                disabled={!canComplete}
            ></AnchorButton>
        </Tooltip>
    );

    return (
        <Popover
            isOpen={showPopover}
            target={completeTaskButton}
            content={<WithGraphQL task={task} hidePopover={() => setShowPopover(false)} />}
        />
    );
};
