import { Button, Card, Collapse, Elevation, H3 } from '@blueprintjs/core';
import { QUERIES } from '@flyacts/estimate-ts-shared';
import { Task, TaskState } from '@flyacts/estimate-ts-shared';
import React, { useState } from 'react';
import { adopt } from 'react-adopt';
import { Query } from 'react-apollo';
import { EstimateList } from '..';
import { ManageTask } from '../ManageTask/ManageTask';
import styles from './TaskItem.module.css';
// tslint:disable:no-shadowed-variable

interface RenderProps {
    render: any;
    taskId: string;
}

const currentParticipantQuery = ({ render }: RenderProps) => (
    <Query query={QUERIES.GET_CURRENT_PARTICIPANT}>{render}</Query>
);

const EstimatesWithCurrentParticipant = adopt({
    currentParticipantQuery,
});

export interface TaskItemProps {
    task: Task;
}

export const TaskItem = ({ task }: TaskItemProps) => {
    const showEstimationListVisibilityToggle = task.state === TaskState.COMPLETE;
    const showFinalEstimation = task.state === TaskState.COMPLETE;
    const [showEstimationList, setShowEstimationList] = useState(false);
    const taskIsActive = task.state === TaskState.ACTIVE;

    const ToggleEstimationView = () => (
        <Button
            icon="arrow-down"
            minimal={true}
            onClick={() => setShowEstimationList(!showEstimationList)}
        ></Button>
    );

    return (
        <EstimatesWithCurrentParticipant
            taskId={task.id}
        >{({ currentParticipantQuery }: any) => {
            const isManager = currentParticipantQuery.data.currentParticipant
                && currentParticipantQuery.data.currentParticipant.isManager;
            return (
                <Card
                    key={task.id}
                    className={styles.card}
                    elevation={taskIsActive ? Elevation.TWO : Elevation.ZERO}
                >
                    <div className={styles.header}>
                        <div className={styles.headerLeftItem}>
                            <H3>
                                {task.name}
                                { showFinalEstimation && <> | {task.estimation} SP</> }
                            </H3>
                            { showEstimationListVisibilityToggle && <ToggleEstimationView/> }
                        </div>
                        { isManager && <ManageTask task={task}></ManageTask> }
                    </div>
                    <Collapse isOpen={showEstimationList || taskIsActive}>
                        <div style={{
                            display: 'flex',
                        }}>
                            <EstimateList task={task}/>
                        </div>
                    </Collapse>
                </Card>
            );
        }}</EstimatesWithCurrentParticipant>
    );
};
