import { TaskListContainer } from '@flyacts/estimate-ts-shared';
import { Task } from '@flyacts/estimate-ts-shared';
import React from 'react';
import { Loading } from '..';
import { TaskItem } from '../TaskItem';

interface TaskListProps {
    sessionId: string;
}

export const TaskList = ({ sessionId }: TaskListProps) => (
    <TaskListContainer
        sessionId={sessionId}
        loading={<Loading />}
    >{(tasks) => tasks.map((task: Task) =>
        <TaskItem key={task.id} task={task}/>,
    )}</TaskListContainer>
);
