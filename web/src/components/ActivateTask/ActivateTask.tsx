import { AnchorButton, Tooltip } from '@blueprintjs/core';
import { MUTATIONS, Task, TaskState } from '@flyacts/estimate-ts-shared';
import React from 'react';
import { Mutation } from 'react-apollo';

export interface ActivateTaskProps {
    task: Task;
}

export const ActivateTask = ({ task }: ActivateTaskProps) => {
    const canActivate = task.state === TaskState.INACTIVE;
    const isAlreadyActive = task.state === TaskState.ACTIVE;
    const toolTipText = canActivate
        ? 'Activate this task'
        : isAlreadyActive
        ? 'This task is already active'
        : 'This task is already complete';
    return (
        <Tooltip content={toolTipText}>
            <Mutation
                mutation={MUTATIONS.ACTIVATE_TASK}
                variables={{ taskId: task.id }}
            >{(activateTask) =>
                <AnchorButton
                    icon="play"
                    intent="primary"
                    onClick={() => activateTask()}
                    disabled={!canActivate}
                ></AnchorButton>
            }</Mutation>
        </Tooltip>
    );
};
