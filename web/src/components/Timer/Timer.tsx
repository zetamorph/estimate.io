import React, { useEffect, useState } from 'react';
import { differenceInSeconds, secondsToTime } from '../../util';

export interface TimerProps {
    endTime: Date;
}

export const Timer = ({ endTime }: TimerProps) => {
    const [timeLeft, setTimeLeft] = useState(differenceInSeconds(new Date(), endTime));

    useEffect(() => {
        const intervalId = setInterval(() => {
            setTimeLeft(differenceInSeconds(new Date(), endTime));
        }, 1000);
        return () => clearInterval(intervalId);
    });

    const time = secondsToTime(timeLeft);

    return <p>{time.hours}:{time.minutes}:{time.seconds}</p>;
};
