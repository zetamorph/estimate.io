import { AnchorButton, Tooltip } from '@blueprintjs/core';
import { MUTATIONS } from '@flyacts/estimate-ts-shared';
import { Task, TaskState } from '@flyacts/estimate-ts-shared';
import React from 'react';
import { Mutation } from 'react-apollo';
import { logger } from '../../logger';

export interface RevealEstimatesProps {
    task: Task;
}

export const RevealEstimates = ({ task }: RevealEstimatesProps) => {
    const taskIsActive = task.state === TaskState.ACTIVE;
    const canReveal = taskIsActive && !task.estimatesVisible;

    const tooltipContent = canReveal
        ? 'Reveal all participants´ estimations'
        : task.estimatesVisible
        ? 'The estimations for this task are already revealed'
        : 'This task is not currently active';

    return (
        <Tooltip content={tooltipContent}>
            <Mutation
                mutation={MUTATIONS.REVEAL_ESTIMATES}
                variables={{ taskId: task.id }}
            >{(revealEstimates) =>
                <AnchorButton
                    icon="eye-open"
                    intent="primary"
                    onClick={() => revealEstimates().catch(logger.error)}
                    disabled={!canReveal}
                ></AnchorButton>
            }</Mutation>
        </Tooltip>
    );
};
