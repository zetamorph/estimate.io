import React from 'react';

export const NetworkError = () => (
    <div>
        There seems to be a problem on our end.
        <br/>
        Please try again later.
    </div>
);
