import { Card, H1, H6, Icon } from '@blueprintjs/core';
import { Estimate } from '@flyacts/estimate-ts-shared';
import React from 'react';
import styles from './EstimateItem.module.css';

export interface EstimateItemProps {
    interactive?: boolean;
    value: Estimate;
    onCardClick?(): void;
}

export const EstimateItem = ({
    value,
    interactive = false,
    onCardClick,
}: EstimateItemProps) => {
    const getCardValue = () => {
        switch (value) {
            case Estimate.UNKNOWN:
                return <Icon icon="help" iconSize={Icon.SIZE_LARGE}></Icon>;
            case Estimate.BREAK:
                return <Icon icon="pause" iconSize={Icon.SIZE_LARGE}></Icon>;
            default:
                return <H1>{value}</H1>;
        }
    };

    return (
        <Card
            className={styles.card}
            interactive={interactive}
            onClick={onCardClick}
        >
            {getCardValue()}
            <H6 className={styles.participantName}>Name</H6>
        </Card>
    );
};
