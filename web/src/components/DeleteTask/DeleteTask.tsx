import { Button, Tooltip } from '@blueprintjs/core';
import { MUTATIONS } from '@flyacts/estimate-ts-shared';
import { Task } from '@flyacts/estimate-ts-shared';
import React from 'react';
import { Mutation } from 'react-apollo';
import { logger } from '../../logger';

export interface DeleteTaskProps {
    task: Task;
}

export const DeleteTask = ({ task }: DeleteTaskProps) => {
    const tootipContent = 'Delete this task';
    return (
        <Tooltip content={tootipContent}>
            <Mutation
                mutation={MUTATIONS.DELETE_TASK}
                variables={{ taskId: task.id }}
            >{(deleteTask) =>
                <Button
                    icon="delete"
                    intent="danger"
                    onClick={() => deleteTask().catch(logger.error)}
                ></Button>
            }</Mutation>
        </Tooltip>
    );
};
