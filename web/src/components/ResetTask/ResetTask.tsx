import { AnchorButton, Tooltip } from '@blueprintjs/core';
import { MUTATIONS } from '@flyacts/estimate-ts-shared';
import { Task, TaskState } from '@flyacts/estimate-ts-shared';
import React from 'react';
import { Mutation } from 'react-apollo';
import { logger } from '../../logger';

export interface ResetTaskProps {
    task: Task;
}

export const ResetTask = ({ task }: ResetTaskProps) => {
    const canReset = task.state === TaskState.ACTIVE;
    const tootipContent = 'Reset this task';

    return (
        <Tooltip content={tootipContent}>
            <Mutation
                mutation={MUTATIONS.RESET_TASK}
                variables={{ taskId: task.id }}
            >{(resetTask) =>
                <AnchorButton
                    icon="refresh"
                    intent="warning"
                    onClick={() => resetTask().catch(logger.error)}
                    disabled={!canReset}
                ></AnchorButton>
            }</Mutation>
        </Tooltip>
    );
};
