import React, { useEffect } from 'react';

interface Props {
    handler: () => void;
}

export const BeforeUnload = ({ handler }: Props) => {
    useEffect(() => {
        window.addEventListener('beforeunload', handler);
        return () => {
            window.removeEventListener('beforeunload', handler);
        };
    });
    return <></>;
};
