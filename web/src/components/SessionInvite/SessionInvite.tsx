import { Button, Intent, Tooltip } from '@blueprintjs/core';
import React, { useState } from 'react';
import { config } from '../../config';

export interface SessionInviteProps {
    sessionId: string;
}

export const SessionInvite = ({ sessionId }: SessionInviteProps) => {
    const inviteLink = `${config.url}/join/${sessionId}`;
    const [copySuccess, setCopySuccess] = useState(false);
    const [currentTimeoutId, setCurrentTimeoutId] = useState<NodeJS.Timeout | undefined>(undefined);

    const handleCopyEvent = (copyEvent: ClipboardEvent) => {
        copyEvent.preventDefault();
        copyEvent.clipboardData!.clearData();
        copyEvent.clipboardData!.setData('text/plain', inviteLink);
        document.removeEventListener('copy', handleCopyEvent, true);
    };

    const copyLinkToClipBoard = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
        document.addEventListener('copy', handleCopyEvent);
        document.execCommand('copy');
        e.currentTarget.focus();
        clearTimeout(currentTimeoutId!);
        const timeoutId = setTimeout(() => setCopySuccess(false), 1500);
        setCopySuccess(true);
        setCurrentTimeoutId(timeoutId);
    };

    const resetToolTip = () => setTimeout(() => {
        clearTimeout(currentTimeoutId!);
        setCopySuccess(false);
        setCurrentTimeoutId(undefined);
    }, Tooltip.defaultProps.transitionDuration);

    return (
        <>
            <Tooltip
                content={copySuccess ? 'Copied!' : 'Copy the invite link'}
                disabled={false}
                enforceFocus={true}
                intent={copySuccess ? 'success' : 'none'}
            >
                <Button
                    disabled={false}
                    icon="link"
                    intent={Intent.PRIMARY}
                    minimal={true}
                    onClick={copyLinkToClipBoard}
                    onMouseLeave={resetToolTip}
                />
            </Tooltip>
        </>
    );
};
