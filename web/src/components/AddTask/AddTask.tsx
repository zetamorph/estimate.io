import { Button, Classes, ControlGroup, InputGroup } from '@blueprintjs/core';
import { MUTATIONS } from '@flyacts/estimate-ts-shared';
import React, { useRef, useState } from 'react';
import { Mutation, MutationFn } from 'react-apollo';
import { logger } from '../../logger';
import { isWhitespaceOrEmpty } from '../../util';
import styles from './AddTask.module.css';

export interface AddTaskProps {
    sessionId: string;
}

export const AddTask = ({ sessionId }: AddTaskProps) => {
    const [taskName, setTaskName] = useState('');
    const inputRef = useRef<HTMLInputElement>(null);
    const formIsInvalid = isWhitespaceOrEmpty(taskName);

    const resetInput = () => {
        setTaskName('');
        inputRef.current!.focus();
    };

    const handleSubmit =  (addTask: MutationFn<any, any>) =>
        (event: React.FormEvent<HTMLFormElement>) => {
            event.preventDefault();
            addTask()
            .then(resetInput)
            .catch(logger.error);
        };

    return (
        <Mutation
            mutation={MUTATIONS.ADD_TASK}
            variables={{
                name: taskName,
                sessionId,
            }}
        >{(addTask) => (
            <form
                onSubmit={handleSubmit(addTask)}
                className={styles.addTaskForm}
            >
                <ControlGroup fill={true}>
                    <InputGroup

                        placeholder="Enter a name for the new task"
                        value={taskName}
                        onChange={(e: any) => setTaskName(e.target.value)}
                        inputRef={inputRef as any}
                    />
                    <Button
                        className={[styles.submitButton, Classes.FIXED].join(' ')}
                        type="submit"
                        intent="primary"
                        disabled={formIsInvalid}
                    >Add Task</Button>
                </ControlGroup>
            </form>
        )}</Mutation>
    );
};
