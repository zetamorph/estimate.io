import { QUERIES, SUBSCRIPTIONS } from '@flyacts/estimate-ts-shared';
import { Estimate, Estimation, Participant, Task, TaskState } from '@flyacts/estimate-ts-shared';
import React from 'react';
import { adopt } from 'react-adopt';
import { Query, Subscription } from 'react-apollo';
import { Loading } from '..';
import { EstimateItem } from '..';
import { AddEstimate } from '../AddEstimate/AddEstimate';
import styles from './EstimateList.module.css';
// tslint:disable:no-shadowed-variable

export interface EstimateListProps {
    task: Task;
}

export interface SubscriptionsWithParticipantProps {
    render?: any;
    taskId: string;
    sessionId: string;
}

const participantsSubscription = ({ sessionId, render }: SubscriptionsWithParticipantProps) => (
    <Subscription
        subscription={SUBSCRIPTIONS.GET_PARTICIPANTS}
        variables={{ sessionId }}
    >{render}</Subscription>
);

const estimatesSubscription = ({ render, taskId }: SubscriptionsWithParticipantProps) => (
    <Subscription
        subscription={SUBSCRIPTIONS.GET_ESTIMATES}
        variables={{ taskId }}
    >{render}</Subscription>
);

const currentParticipantQuery = ({ render }: SubscriptionsWithParticipantProps) => (
    <Query
        query={QUERIES.GET_CURRENT_PARTICIPANT}
    >{render}</Query>
);

const SubscriptionsWithParticipant = adopt({
    currentParticipantQuery,
    estimatesSubscription,
    participantsSubscription,
});

export const EstimateList = ({ task }: EstimateListProps) => {
    return (<div className={styles.estimateList}>
        <SubscriptionsWithParticipant
            taskId={task.id}
            sessionId={task.sessionId}>{({
                estimatesSubscription,
                participantsSubscription,
                currentParticipantQuery,
            }: any) => {
                if (
                    estimatesSubscription.loading ||
                    participantsSubscription.loading ||
                    currentParticipantQuery.loading
                ) {
                    return <Loading />;
                }
                const currentParticipant: Participant = currentParticipantQuery.data.currentParticipant;
                const estimates: Estimation[] = estimatesSubscription.data.estimationServiceListenEstimates.estimates;
                const hasNotVoted = !estimates.find((e) => e.participantId === currentParticipant.id);
                const taskIsActive = task.state === TaskState.ACTIVE;
                const { isManager } = currentParticipant;
                const { estimatesVisible } = task;

                const canVote = hasNotVoted && taskIsActive && !isManager;

                return (<>{
                    canVote
                        ? Object.values(Estimate)
                            .filter((value) => typeof value === 'number')
                            .map((value: number) => <AddEstimate
                                key={value}
                                taskId={task.id}
                                participantId={currentParticipant.id}
                                value={value}
                            />)
                        : estimates.map((estimate: Estimation) => (
                            <EstimateItem
                                key={estimate.id}
                                value={estimatesVisible ? estimate.estimate : Estimate.UNKNOWN}
                            ></EstimateItem>
                        ))
                }</>);
            }
        }</SubscriptionsWithParticipant>
    </div>);
};
