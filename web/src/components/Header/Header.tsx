import { Alignment, Button, Navbar } from '@blueprintjs/core';
import { QUERIES } from '@flyacts/estimate-ts-shared';
import { Session } from '@flyacts/estimate-ts-shared';
import { addSeconds } from 'date-fns';
import React from 'react';
import { Query } from 'react-apollo';
import { Link } from 'react-router-dom';
import { SessionInvite } from '..';
import { Timer } from '..';
import styles from './Header.module.css';

const CurrentSession = () => (
    <Query<{ currentSession: Session } | null>
        query={QUERIES.GET_CURRENT_SESSION}
    >{({ data }) => {
        if (!(data && data.currentSession && data.currentSession.id)) {
            return <></>;
        }

        const {
            createdAt,
            id,
            name,
            timebox: { value: sessionTimebox },
        } = data.currentSession;

        const endTime = sessionTimebox
            ? addSeconds(createdAt.seconds * 1000, sessionTimebox)
            : null;

        return (
            <>
                <Navbar.Divider />
                <div className={styles.currentSession}>
                    <Navbar.Heading>{name}</Navbar.Heading>
                    <SessionInvite sessionId={id} />
                </div>
                {endTime && <Timer endTime={endTime} />}
            </>
        );
    }}</Query>
);

export const Header = () => <>
    <Navbar>
        <Navbar.Group align={Alignment.LEFT}>
            <Navbar.Heading>
                <Link to="/">estimate</Link>
            </Navbar.Heading>
            <CurrentSession />
        </Navbar.Group>
        <Navbar.Group align={Alignment.RIGHT}>
            <Link to="/">
                <Button minimal={true} icon="log-out">Leave</Button>
            </Link>
        </Navbar.Group>
    </Navbar>
</>;
