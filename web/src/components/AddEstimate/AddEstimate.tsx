import { MUTATIONS } from '@flyacts/estimate-ts-shared';
import React from 'react';
import { Mutation } from 'react-apollo';
import { EstimateItem } from '..';
import { logger } from '../../logger';

export interface AddEstimateProps {
    taskId: string;
    participantId: string;
    value: number;
}

export const AddEstimate = ({ taskId, participantId, value }: AddEstimateProps) => <>
    <Mutation
        mutation={MUTATIONS.ADD_ESTIMATE}
        variables={{
            participantId,
            taskId,
            value,
        }}
    >{(addEstimate) =>
        <EstimateItem
            value={value}
            interactive={true}
            onCardClick={() => addEstimate().catch(logger.error)}
        ></EstimateItem>
    }</Mutation>
</>;
