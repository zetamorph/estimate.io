import { ButtonGroup } from '@blueprintjs/core';
import { Task } from '@flyacts/estimate-ts-shared';
import React from 'react';
import { ActivateTask, CompleteTask, DeleteTask, ResetTask, RevealEstimates } from '..';

export interface ManageTaskProps {
    task: Task;
}

export const ManageTask = ({ task }: ManageTaskProps) => {
    return (
        <div>
            <ButtonGroup>
                <ActivateTask task={task} />
                <CompleteTask task={task} />
                <ResetTask task={task} />
                <RevealEstimates task={task} />
                <DeleteTask task={task} />
            </ButtonGroup>
        </div>
    );
};
