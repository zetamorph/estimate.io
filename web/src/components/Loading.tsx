import { Spinner } from '@blueprintjs/core';
import React from 'react';

export const Loading = () => (
    <div style={{ padding: '1em' }}>
        <Spinner
            intent="primary"
            size={Spinner.SIZE_SMALL}
        />
    </div>
);
