import { Card, Icon } from '@blueprintjs/core';
import { Participant } from '@flyacts/estimate-ts-shared';
import React from 'react';
import styles from './ParticipantItem.module.css';

export interface ParticipantItemProps {
    participant: Participant;
    isSelf: boolean;
}

export const ParticipantItem = ({ participant: { name, isManager }, isSelf }: ParticipantItemProps) => (
    <>
        <Card
            className={styles.card}
        >
            <img className={styles.cardImage}
                src={`https://ui-avatars.com/api/?name=${encodeURIComponent(name)}&rounded=true`}
                ></img>
            <div className={styles.cardContent}>
                <h4 className={styles.cardHeading}>
                    {isSelf ? 'You' : name}
                </h4>
                <div className={styles.cardIcons}>
                    {isManager
                        ? <Icon icon="crown" iconSize={20}></Icon>
                        : <></>
                    }
                </div>
            </div>
        </Card>
    </>
);
