import { ParticipantListContainer } from '@flyacts/estimate-ts-shared';
import { Participant } from '@flyacts/estimate-ts-shared';
import React from 'react';
import { Loading } from '..';
import { ParticipantItem } from '../ParticipantItem';
import styles from './ParticipantList.module.css';

export interface ParticipantListProps {
    sessionId: string;
}

export const ParticipantList = ({ sessionId }: ParticipantListProps) => (
    <div className={styles.participantList}>
        <ParticipantListContainer
            sessionId={sessionId}
            loading={<Loading />}
        >{(participants: Participant[], currentParticipant: Participant) => (
            participants.map((participant) => {
                const isSelf = currentParticipant.id === participant.id;
                return (
                    <ParticipantItem
                        key={participant.id}
                        isSelf={isSelf}
                        participant={participant}
                    />
                );
            })
        )}</ParticipantListContainer>
    </div>
);
