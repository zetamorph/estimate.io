import { Button, ControlGroup, InputGroup } from '@blueprintjs/core';
import { MUTATIONS } from '@flyacts/estimate-ts-shared';
import { UPDATE_CURRENT_PARTICIPANT_AND_SESSION } from '@flyacts/estimate-ts-shared';
import React from 'react';
import { useState } from 'react';
import { Mutation, MutationFn } from 'react-apollo';
import { RouteComponentProps, withRouter } from 'react-router';
import { JoinSessionRouteProps } from '../../App';
import { logger } from '../../logger';

export const JoinSession = withRouter(({ match, history }: RouteComponentProps<JoinSessionRouteProps>) => {
    const sessionId = match.params.sessionId;
    const [participantName, setParticipantName] = useState('');
    const formIsInvalid = !participantName;

    const handleSubmit = (joinSession: MutationFn<any, any>) => () => {
        joinSession()
        .then(() => history.push(`/sessions/${sessionId}`))
        .catch(logger.error);
    };

    return (
        <Mutation
            mutation={MUTATIONS.JOIN_SESSION}
            update={UPDATE_CURRENT_PARTICIPANT_AND_SESSION('estimationServiceJoinSession')}
            variables={{
                participantName,
                sessionId,
            }}
        >{(joinSession) => (
            <main>
                <h2>Join an existing session</h2>
                <section>
                    <ControlGroup>
                        <InputGroup
                            placeholder="Enter your name"
                            value={participantName}
                            maxLength={255}
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => setParticipantName(e.target.value)}
                        >
                        </InputGroup>
                        <Button
                            disabled={formIsInvalid}
                            onClick={handleSubmit(joinSession)}
                        >Join</Button>
                    </ControlGroup>
                </section>
            </main>
        )}</Mutation>
    );
});
