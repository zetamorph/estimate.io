import { MUTATIONS, Participant, QUERIES } from '@flyacts/estimate-ts-shared';
import React from 'react';
import { Query } from 'react-apollo';
import { Prompt, RouteComponentProps, withRouter } from 'react-router';
import { AddTask, BeforeUnload, Loading, ParticipantList, TaskList } from '../../components';
import { config } from '../../config';
import { LeaveSessionGuardData } from '../../guards';
import { logger } from '../../logger';
import styles from './Session.module.css';

export const SessionPage = withRouter(({ history, match }: RouteComponentProps<{ sessionId: string }>) => {
    const sessionId = match.params.sessionId;

    // tslint:disable-next-line: no-shadowed-variable
    const leaveSession = (sessionId: string, participantId: string) => {
        const query = JSON.stringify({
            operationName: 'EstimationServiceLeaveSession',
            query: MUTATIONS.LEAVE_SESSION,
            variables: {
                participantId,
                sessionId,
            },
        });

        const blob = new Blob(
            [query],
            {type : 'application/json; charset=UTF-8'},
        );
        navigator.sendBeacon(config.graphqlHttpUrl, blob);
    };

    return (<>
        <Query<{ currentParticipant: Participant }> query={QUERIES.GET_CURRENT_PARTICIPANT}>
            {({ loading, data, error }) => {
                if (loading) {
                    return <Loading/>;
                }
                if (error) {
                    logger.error(error);
                    history.push('/');
                }

                if (!data!.currentParticipant && typeof sessionId === 'string') {
                    history.push(`/join/${sessionId}`);
                }

                const { isManager, id: participantId } = data!.currentParticipant;
                return (<>
                    <section className={styles.sessionPage}>
                        <aside className={styles.participantList}>
                            <ParticipantList sessionId={sessionId}></ParticipantList>
                        </aside>
                        <main className={styles.taskList}>
                            { isManager && <AddTask sessionId={sessionId}></AddTask> }
                            <TaskList sessionId={sessionId}></TaskList>
                        </main>
                    </section>
                    <BeforeUnload handler={() => leaveSession(sessionId, participantId)}></BeforeUnload>
                    <Prompt
                        when={true}
                        message={JSON.stringify({
                            message: 'Do you really want to leave this session?',
                            participantId,
                            sessionId,
                            type: '@@LeaveSession',
                        } as LeaveSessionGuardData)}
                    />
                </>);
            }}
        </Query>
    </>);
});
