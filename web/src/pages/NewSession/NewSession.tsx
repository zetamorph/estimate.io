import { Button, FormGroup, InputGroup } from '@blueprintjs/core';
import { TimePicker } from '@blueprintjs/datetime';
import { MUTATIONS } from '@flyacts/estimate-ts-shared';
import { UPDATE_CURRENT_PARTICIPANT_AND_SESSION } from '@flyacts/estimate-ts-shared';
import React, { useState } from 'react';
import { Mutation, MutationFn } from 'react-apollo';
import { RouteComponentProps, withRouter } from 'react-router';
import { config } from '../../config';
import { logger } from '../../logger';
import { isWhitespaceOrEmpty } from '../../util';

/**
 * The BlueprintJS TimePicker returns a native Date Object
 * with only hours, minutes and seconds set.
 */
const getTimeboxSeconds = (date: Date): number => (
        date.getSeconds() +
        date.getMinutes() * 60 +
        date.getHours() * 60 * 60
    );

const NewSession = ({ history }: RouteComponentProps) => {
    const [managerName, setManagerName] = useState('');
    const [sessionName, setSessionName] = useState('');
    const [taskTimebox, setTaskTimebox] = useState<Date | undefined>(undefined);
    const [timebox, setTimebox] = useState<Date | undefined>(undefined);

    const formIsInvalid = isWhitespaceOrEmpty(sessionName) || isWhitespaceOrEmpty(managerName);

    const handleSubmit = (createSession: MutationFn<any, any>) =>
        (event: React.FormEvent<HTMLFormElement>) => {
            event.preventDefault();
            createSession()
            .then(({ data }: any) => {
                const { session } = data.estimationServiceCreateSession;
                history.push(`/sessions/${session.id}`);
            })
            .catch(logger.error);
        };

    return (
        <Mutation
            mutation={MUTATIONS.CREATE_SESSION}
            variables={{
                managerName,
                sessionName,
                taskTimebox: { value: taskTimebox
                    ? getTimeboxSeconds(taskTimebox)
                    : null,
                },
                timebox: { value: timebox
                    ? getTimeboxSeconds(timebox)
                    : null,
                },
            }}
            update={UPDATE_CURRENT_PARTICIPANT_AND_SESSION('estimationServiceCreateSession')}
        >{(createSession) => (
            <section>
                <form onSubmit={handleSubmit(createSession)}>
                    <h2>Start a new session</h2>
                    <FormGroup
                        label="Session Name"
                        labelInfo="(required)"
                    >
                        <InputGroup
                            placeholder="Enter a name for the session"
                            maxLength={255}
                            value={sessionName}
                            onChange={(event: any) => setSessionName(event.target.value)}
                        />
                    </FormGroup>
                    <FormGroup
                        label="Your Name"
                        labelInfo="(required)"
                    >
                        <InputGroup
                            placeholder="Enter your name"
                            maxLength={255}
                            value={managerName}
                            onChange={(event: any) => setManagerName(event.target.value)}
                        />
                    </FormGroup>
                    {config.features.taskTimebox
                        ? <FormGroup
                            disabled={true}
                            label="Task Timebox"
                            labelInfo="(optional)"
                        >
                            <TimePicker
                                disabled={true}
                                showArrowButtons={true}
                                value={taskTimebox}
                                precision="second"
                                onChange={setTaskTimebox}
                            />
                        </FormGroup>
                        : <></>
                    }
                    {config.features.taskTimebox
                        ? <FormGroup
                            disabled={true}
                            label="Session Timebox"
                            labelInfo="(optional)"
                        >
                            <TimePicker
                                disabled={true}
                                showArrowButtons={true}
                                value={timebox}
                                precision="second"
                                onChange={setTimebox}
                            />
                        </FormGroup>
                        : <></>
                    }
                    <Button
                        fill={true}
                        type="submit"
                        intent="primary"
                        disabled={formIsInvalid}
                    >Start</Button>
                </form>
            </section>
        )}</Mutation>
    );
};

export default withRouter(NewSession);
