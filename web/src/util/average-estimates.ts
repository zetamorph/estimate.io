import { Estimate, Estimation } from '@flyacts/estimate-ts-shared';

const filterNumberEstimates = (e: Estimate) => ![
    Estimate.UNKNOWN,
    Estimate.BREAK,
].includes(e);

export const averageEstimates = (estimates: Estimation[]): number => {
    const relevantEstimations = estimates
        .map((e) => e.estimate)
        .filter(filterNumberEstimates);

    return Math.round(relevantEstimations
        .reduce((acc, curr) => acc + curr, 0)
        / relevantEstimations.length);
};
