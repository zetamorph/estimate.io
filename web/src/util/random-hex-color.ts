/* Taken from https://www.paulirish.com/2009/random-hex-color-code-snippets/ */
export const randomHexColor = () => '#' + Math.floor(Math.random() * 16777215).toString(16);
