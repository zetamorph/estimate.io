export interface Time {
    hours: number;
    minutes: number;
    seconds: number;
}

export const secondsToTime = (seconds: number): Time => {
    const result: Time = {
        hours: 0,
        minutes: 0,
        seconds: 0,
    };
    while (seconds >= 3600) {
        result.hours++;
        seconds -= 3600;
    }
    while (seconds >= 60) {
        result.minutes++;
        seconds -= 60;
    }
    result.seconds = Math.floor(seconds);
    return result;
};

export const differenceInSeconds = (a: Date, b: Date) => {
    const aSeconds = a.getTime() / 1000;
    const bSeconds = b.getTime() / 1000;
    return bSeconds - aSeconds;
};
