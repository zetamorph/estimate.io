export const isWhitespaceOrEmpty = (s: string) => s
    .replace(/\s/g, '')
    .length === 0;
