import React from 'react';
import { Route, Switch } from 'react-router-dom';
import styles from './App.module.css';
import { Header } from './components';
import { JoinSession } from './pages/JoinSession/JoinSession';
import NewSession from './pages/NewSession/NewSession';
import { SessionPage } from './pages/Session/Session';

export interface JoinSessionRouteProps {
  sessionId: string;
}

export const App = () => {
  return (
    <>
      <Header />
      <div className={styles.content}>
        <Switch>
          <Route exact path="/" component={NewSession}/>
          <Route exact path="/join/:sessionId" component={JoinSession} />
          <Route exact path="/sessions/:sessionId" component={SessionPage} />
        </Switch>
      </div>
    </>
  );
};
