export interface Config {
    features: {
        sessionTimebox: boolean;
        taskTimebox: boolean;
    };
    graphqlHttpUrl: string;
    graphqlWsUrl: string;
    url: string;
}

declare global {
    interface Window {
        ENV: {
            FEATURES: {
                SESSION_TIMEBOX: boolean;
                TASK_TIMEBOX: boolean;
            },
            GRAPHQL_HTTP: string;
            GRAPHQL_WS: string;
            URL: string;
        };
    }
}

export const config: Config = {
    features: {
        sessionTimebox: window.ENV.FEATURES.SESSION_TIMEBOX || false,
        taskTimebox: window.ENV.FEATURES.SESSION_TIMEBOX || false,
    },
    graphqlHttpUrl: window.ENV.GRAPHQL_HTTP,
    graphqlWsUrl: window.ENV.GRAPHQL_WS,
    url: window.ENV.URL,
};
