import { Intent, Toaster } from '@blueprintjs/core';
import { Participant, Session } from '@flyacts/estimate-ts-shared';
import { defaults, resolvers } from '@flyacts/estimate-ts-shared';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { persistCache } from 'apollo-cache-persist';
import { ApolloClient } from 'apollo-client';
import { from, split } from 'apollo-link';
import { onError } from 'apollo-link-error';
import { HttpLink } from 'apollo-link-http';
import { withClientState } from 'apollo-link-state';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';
import { OperationDefinitionNode } from 'graphql';
import React from 'react';
import { NetworkError } from './components';
import { config } from './config';

function WebsocketWithProtocol(url: string) {
  return new WebSocket(url, 'graphql-ws');
}

// Create an http link:
const httpLink = new HttpLink({
  uri: config.graphqlHttpUrl,
});

// Create a WebSocket link:
const wsLink = new WebSocketLink({
  options: {
    reconnect: true,
    timeout: 30000,
  },
  uri: config.graphqlWsUrl,
  webSocketImpl: false ? WebsocketWithProtocol : undefined,
});

const toaster = Toaster.create(undefined, document.body);

const errorLink = onError(({ graphQLErrors, networkError }) => {
  const intent: Intent = 'danger';
  const message = networkError
    ? <NetworkError/>
    : graphQLErrors!.join('\n');
  toaster.show({ message, intent });
});

const cache = new InMemoryCache();

persistCache({
  cache,
  storage: window.sessionStorage as any,
});

const stateLink = withClientState({
  cache,
  defaults,
  resolvers,
});

const remoteLink = split(
  // split based on operation type
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query) as OperationDefinitionNode;
    return kind === 'OperationDefinition' && operation === 'subscription';
  },
  wsLink,
  httpLink,
);

export const client = new ApolloClient({
  cache,
  link: from([
    stateLink,
    errorLink,
    remoteLink,
  ]),
});

export interface ClientState {
  currentSession: Session | null;
  currentParticipant: Participant | null;
  clientId: string;
}
