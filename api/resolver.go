package api

import (
	"context"
	"fmt"
	"io"
	"log"

	types "github.com/gogo/protobuf/types"
	"github.com/zetamorph/estimate-io/proto"
)

// ProtoInt64ToGQLInt64 converts a Protobuf Int64 wrapper to a GQL Int64 wrapper
func ProtoInt64ToGQLInt64(v *types.Int64Value) *Int64Value {
	conv := &Int64Value{}
	if v != nil {
		converted := int(v.Value)
		conv.Value = &converted
	}
	return conv
}

// GQLInt64ToProtoInt64 converts a GQL Int64 wrapper to a Protobuf Int64 wrapper
func GQLInt64ToProtoInt64(v *Int64Value) *types.Int64Value {
	conv := &types.Int64Value{}
	if v.Value != nil {
		converted := int64(*v.Value)
		conv.Value = converted
	}
	return conv
}

// GQLInt64InputToProtoInt64 converts a GQL Int64ValueInput wrapper to a Protobuf Int64 wrapper
func GQLInt64InputToProtoInt64(v *Int64ValueInput) *types.Int64Value {
	conv := &types.Int64Value{}
	if v.Value != nil {
		converted := int64(*v.Value)
		conv.Value = converted
	}
	return conv
}

// ProtoSessionToGQL Converts a Protobuf Session struct to a GQLGen struct
func ProtoSessionToGQL(s *proto.Session) *Session {
	timebox := ProtoInt64ToGQLInt64(s.Timebox)
	taskTimebox := ProtoInt64ToGQLInt64(s.TaskTimebox)
	createdAt := Timestamp{}
	seconds := int(s.CreatedAt.Seconds)
	nanos := int(s.CreatedAt.Nanos)
	createdAt.Seconds = &seconds
	createdAt.Nanos = &nanos

	return &Session{
		ID:          &s.Id,
		Name:        &s.Name,
		Timebox:     timebox,
		TaskTimebox: taskTimebox,
		CreatedAt:   &createdAt,
	}
}

// GQLSessionToProto Converts a GQL Session struct to a Proto struct
func GQLSessionToProto(s *Session) *proto.Session {
	timebox := types.Int64Value{}
	if s.Timebox.Value != nil {
		converted := int64(*s.Timebox.Value)
		timebox.Value = converted
	}
	taskTimebox := types.Int64Value{}
	if s.TaskTimebox.Value != nil {
		converted := int64(*s.TaskTimebox.Value)
		taskTimebox.Value = converted
	}

	return &proto.Session{
		Id:          *s.ID,
		Name:        *s.Name,
		Timebox:     &timebox,
		TaskTimebox: &taskTimebox,
	}
}

// Resolver struct
type Resolver struct {
	EstimationService proto.EstimationService
}

// Mutation Resolver
func (r *Resolver) Mutation() MutationResolver {
	return &mutationResolver{r}
}

// Query Resolver
func (r *Resolver) Query() QueryResolver {
	return &queryResolver{r}
}

// Subscription Resolver
func (r *Resolver) Subscription() SubscriptionResolver {
	return &subscriptionResolver{r}
}

type mutationResolver struct{ *Resolver }

func (r *mutationResolver) EstimationServiceResetTask(ctx context.Context, in *ResetTaskRequestInput) (bool, error) {
	_, err := r.Resolver.EstimationService.ResetTask(ctx, &proto.ResetTaskRequest{
		TaskId: *in.TaskID,
	})
	if err != nil {
		return false, err
	}
	return true, nil
}

func (r *mutationResolver) EstimationServiceRevealEstimates(ctx context.Context, in *RevealEstimatesRequestInput) (bool, error) {
	_, err := r.Resolver.EstimationService.RevealEstimates(ctx, &proto.RevealEstimatesRequest{
		TaskId: *in.TaskID,
	})
	if err != nil {
		return false, err
	}
	return true, nil
}

func (r *mutationResolver) EstimationServiceCompleteTask(ctx context.Context, in *CompleteTaskRequestInput) (bool, error) {
	estimation := int32(*in.Estimation)
	_, err := r.Resolver.EstimationService.CompleteTask(ctx, &proto.CompleteTaskRequest{
		TaskId:     *in.TaskID,
		Estimation: estimation,
	})
	if err != nil {
		return false, err
	}
	return true, nil
}

func (r *mutationResolver) EstimationServiceCreateSession(ctx context.Context, in *CreateSessionRequestInput) (*CreateSessionResponse, error) {
	res, err := r.Resolver.EstimationService.CreateSession(ctx, &proto.CreateSessionRequest{
		Name:        *in.Name,
		ManagerName: *in.ManagerName,
		Timebox:     GQLInt64InputToProtoInt64(in.Timebox),
		TaskTimebox: GQLInt64InputToProtoInt64(in.TaskTimebox),
	})
	if err != nil {
		log.Fatalf("%v.CreateSession(_) = _, %v", r.Resolver.EstimationService, err)
		return nil, err
	}
	return &CreateSessionResponse{
		Session: ProtoSessionToGQL(res.Session),
		Participant: &Participant{
			ID:        &res.Participant.Id,
			Name:      &res.Participant.Name,
			IsManager: &res.Participant.IsManager,
			SessionID: &res.Participant.SessionId,
		},
	}, nil
}

func (r *mutationResolver) EstimationServiceJoinSession(ctx context.Context, in *JoinSessionRequestInput) (*JoinSessionResponse, error) {
	res, err := r.Resolver.EstimationService.JoinSession(ctx, &proto.JoinSessionRequest{SessionId: *in.SessionID, ParticipantName: *in.ParticipantName})
	if err != nil {
		log.Fatalf("%v.JoinSession(_) = _, %v", r.Resolver.EstimationService, err)
		return nil, err
	}
	fmt.Printf("isManager: %v", res.Participant.IsManager)
	return &JoinSessionResponse{
		Participant: &Participant{
			ID:        &res.Participant.Id,
			Name:      &res.Participant.Name,
			SessionID: &res.Participant.SessionId,
			IsManager: &res.Participant.IsManager,
		},
		Session: ProtoSessionToGQL(res.Session),
	}, nil
}

func (r *mutationResolver) EstimationServiceLeaveSession(ctx context.Context, in *LeaveSessionRequestInput) (bool, error) {
	_, err := r.Resolver.EstimationService.LeaveSession(ctx, &proto.LeaveSessionRequest{SessionId: *in.SessionID, ParticipantId: *in.ParticipantID})
	if err != nil {
		log.Fatalf("%v.LeaveSession(_) = _, %v", r.Resolver.EstimationService, err)
		return false, err
	}
	return true, nil
}

func (r *mutationResolver) EstimationServiceActivateTask(ctx context.Context, in *ActivateTaskRequestInput) (bool, error) {
	_, err := r.Resolver.EstimationService.ActivateTask(ctx, &proto.ActivateTaskRequest{TaskId: *in.TaskID})
	if err != nil {
		log.Fatalf("%v.ActivateTask(_) = _, %v", r.Resolver.EstimationService, err)
		return false, err
	}
	return true, nil
}

func (r *mutationResolver) EstimationServiceDeleteTask(ctx context.Context, in *DeleteTaskRequestInput) (bool, error) {
	_, err := r.Resolver.EstimationService.DeleteTask(ctx, &proto.DeleteTaskRequest{TaskId: *in.TaskID})
	if err != nil {
		log.Fatalf("%v.DeleteTask(_) = _, %v", r.Resolver.EstimationService, err)
		return false, err
	}
	return true, nil
}

func (r *mutationResolver) EstimationServiceAddTask(ctx context.Context, in *AddTaskRequestInput) (*AddTaskResponse, error) {
	rsp, err := r.Resolver.EstimationService.AddTask(ctx, &proto.AddTaskRequest{SessionId: *in.SessionID, Name: *in.Name})
	if err != nil {
		log.Fatalf("%v.AddTask(_) = _, %v", r.Resolver.EstimationService, err)
		return nil, err
	}
	taskState := TaskState(rsp.Task.State.String())
	estimation := int(rsp.Task.Estimation)
	return &AddTaskResponse{
		Task: &Task{
			ID:               &rsp.Task.Id,
			Name:             &rsp.Task.Name,
			SessionID:        &rsp.Task.SessionId,
			State:            &taskState,
			EstimatesVisible: &rsp.Task.EstimatesVisible,
			Estimation:       &estimation,
		},
	}, nil
}

func (r *mutationResolver) EstimationServiceAddEstimate(ctx context.Context, in *AddEstimateRequestInput) (*AddEstimateResponse, error) {
	rsp, err := r.Resolver.EstimationService.AddEstimate(ctx, &proto.AddEstimateRequest{Estimate: &proto.Estimate{
		TaskId:        *in.Estimate.TaskID,
		ParticipantId: *in.Estimate.ParticipantID,
		Estimate:      int32(*in.Estimate.Estimate),
	}})
	if err != nil {
		log.Fatalf("%v.AddEstimate(_) = _, %v", r.Resolver.EstimationService, err)
		return nil, err
	}
	estimation := int(rsp.Estimate.Estimate)
	return &AddEstimateResponse{
		Estimate: &Estimate{
			ID:            &rsp.Estimate.Id,
			TaskID:        &rsp.Estimate.TaskId,
			ParticipantID: &rsp.Estimate.ParticipantId,
			Estimate:      &estimation,
		},
	}, nil
}

type queryResolver struct{ *Resolver }

func (r *queryResolver) EstimationServiceGetSession(ctx context.Context, in *GetSessionRequestInput) (*GetSessionResponse, error) {
	res, err := r.Resolver.EstimationService.GetSession(ctx, &proto.GetSessionRequest{
		Id: *in.ID,
	})
	if err != nil {
		log.Fatalf("%v.GetSession(_) = _, %v", r.Resolver.EstimationService, err)
		return nil, err
	}
	rsp := &GetSessionResponse{
		Session: ProtoSessionToGQL(res.Session),
	}
	fmt.Printf("%+v\n", rsp)
	return rsp, nil
}

func (r *queryResolver) EstimationServiceGetTasks(ctx context.Context, in *GetTasksRequestInput) (*GetTasksResponse, error) {
	res, err := r.Resolver.EstimationService.GetTasks(ctx, &proto.GetTasksRequest{
		SessionId: *in.SessionID,
	})
	if err != nil {
		log.Fatalf("%v.GetTasks(_) = _, %v", r.Resolver.EstimationService, err)
		return nil, err
	}
	var tasks []Task
	for _, task := range res.Tasks {
		taskState := TaskState(task.State.String())
		estimation := int(task.Estimation)
		tasks = append(tasks, Task{
			ID:               &task.Id,
			Name:             &task.Name,
			SessionID:        &task.SessionId,
			State:            &taskState,
			Estimation:       &estimation,
			EstimatesVisible: &task.EstimatesVisible,
		})
	}
	return &GetTasksResponse{
		Tasks: tasks,
	}, nil
}

func (r *queryResolver) EstimationServiceGetEstimates(ctx context.Context, in *GetEstimatesRequestInput) (*GetEstimatesResponse, error) {
	res, err := r.Resolver.EstimationService.GetEstimates(ctx, &proto.GetEstimatesRequest{
		TaskId: *in.TaskID,
	})
	if err != nil {
		log.Fatalf("%v.GetEstimates(_) = _, %v", r.Resolver.EstimationService, err)
		return nil, err
	}
	var estimates []Estimate
	for _, estimate := range res.Estimates {
		estimation := int(estimate.Estimate)
		estimates = append(estimates, Estimate{
			ID:            &estimate.Id,
			TaskID:        &estimate.TaskId,
			ParticipantID: &estimate.ParticipantId,
			Estimate:      &estimation,
		})
	}
	return &GetEstimatesResponse{
		Estimates: estimates,
	}, nil
}

func (r *queryResolver) EstimationServiceGetParticipants(ctx context.Context, in *GetParticipantsRequestInput) (*GetParticipantsResponse, error) {
	res, err := r.Resolver.EstimationService.GetParticipants(ctx, &proto.GetParticipantsRequest{
		SessionId: *in.SessionID,
	})
	if err != nil {
		log.Fatalf("%v.GetParticipants(_) = _, %v", r.Resolver.EstimationService, err)
		return nil, err
	}
	var participants []Participant
	for _, participant := range res.Participants {
		participants = append(participants, Participant{
			ID:        &participant.Id,
			Name:      &participant.Name,
			SessionID: &participant.SessionId,
			IsManager: &participant.IsManager,
		})
	}
	return &GetParticipantsResponse{
		Participants: participants,
	}, nil
}

type subscriptionResolver struct{ *Resolver }

func (r *subscriptionResolver) EstimationServiceListenParticipants(ctx context.Context, in *GetParticipantsRequestInput) (<-chan *GetParticipantsResponse, error) {
	stream, err := r.Resolver.EstimationService.ListenParticipants(ctx, &proto.GetParticipantsRequest{
		SessionId: *in.SessionID,
	})
	if err != nil {
		log.Fatalf("%v.GetParticipants(_) = _, %v", r.Resolver.EstimationService, err)
	}
	partc := make(chan *GetParticipantsResponse, 10)
	go func() {
		for {
			in, err := stream.Recv()
			if err == io.EOF {
				// read done.
				close(partc)
				return
			}
			if err != nil {
				log.Fatalf("Failed to receive participants : %v", err)
			}
			participants := make([]Participant, len(in.Participants))
			for i, participant := range in.Participants {
				participants[i] = Participant{
					ID:        &participant.Id,
					Name:      &participant.Name,
					SessionID: &participant.SessionId,
					IsManager: &participant.IsManager,
				}
			}
			partc <- &GetParticipantsResponse{
				Participants: participants,
			}
		}
	}()
	return partc, nil
}

func (r *subscriptionResolver) EstimationServiceListenTasks(ctx context.Context, in *GetTasksRequestInput) (<-chan *GetTasksResponse, error) {
	stream, err := r.Resolver.EstimationService.ListenTasks(ctx, &proto.GetTasksRequest{
		SessionId: *in.SessionID,
	})
	if err != nil {
		log.Fatalf("%v.GetTasks(_) = _, %v", r.Resolver.EstimationService, err)
	}
	taskc := make(chan *GetTasksResponse, 10)
	go func() {
		for {
			in, err := stream.Recv()
			fmt.Printf("Received tasks: %v \n", in)
			if err == io.EOF {
				// read done.
				close(taskc)
				return
			}
			if err != nil {
				log.Fatalf("Failed to receive tasks : %v\n", err)
			}
			tasks := make([]Task, len(in.Tasks))
			for i, task := range in.Tasks {
				taskState := TaskState(task.State.String())
				estimation := int(task.Estimation)
				tasks[i] = Task{
					ID:               &task.Id,
					Name:             &task.Name,
					SessionID:        &task.SessionId,
					State:            &taskState,
					Estimation:       &estimation,
					EstimatesVisible: &task.EstimatesVisible,
				}
			}
			taskc <- &GetTasksResponse{
				Tasks: tasks,
			}
		}
	}()
	return taskc, nil
}

func (r *subscriptionResolver) EstimationServiceListenEstimates(ctx context.Context, in *GetEstimatesRequestInput) (<-chan *GetEstimatesResponse, error) {
	stream, err := r.Resolver.EstimationService.ListenEstimates(ctx, &proto.GetEstimatesRequest{
		TaskId: *in.TaskID,
	})
	if err != nil {
		log.Fatalf("%v.GetEstimates(_) = _, %v", r.Resolver.EstimationService, err)
	}
	partc := make(chan *GetEstimatesResponse, 10)
	go func() {
		for {
			in, err := stream.Recv()
			if err == io.EOF {
				// read done.
				close(partc)
				return
			}
			if err != nil {
				log.Fatalf("Failed to receive estimates : %v\n", err)
			}
			estimates := make([]Estimate, len(in.Estimates))
			for i, estimate := range in.Estimates {
				estimation := int(estimate.Estimate)
				estimates[i] = Estimate{
					ID:            &estimate.Id,
					TaskID:        &estimate.TaskId,
					ParticipantID: &estimate.ParticipantId,
					Estimate:      &estimation,
				}
			}
			partc <- &GetEstimatesResponse{
				Estimates: estimates,
			}
		}
	}()
	return partc, nil
}
