package main

import (
	"context"
	"fmt"
	"net/http"
	"os"

	"github.com/99designs/gqlgen/handler"
	"github.com/go-chi/chi"
	"github.com/gorilla/websocket"
	micro "github.com/micro/go-micro"
	"github.com/micro/go-micro/client"
	"github.com/rs/cors"
	"github.com/zetamorph/estimate-io/api"
	"github.com/zetamorph/estimate-io/proto"
)

const defaultPort = "8080"

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	service := micro.NewService(micro.Name("estimation.client"))
	service.Init()
	estimationClient := service.Client()
	estimationClient.Init(
		client.Retry(client.RetryFunc(func(ctx context.Context, req client.Request, retryCount int, err error) (bool, error) {
			fmt.Println(err)
			return true, nil
		})),
	)
	estimationService := proto.NewEstimationService("estimation", estimationClient)

	router := chi.NewRouter()

	router.Use(cors.New(cors.Options{
		AllowedOrigins:   []string{"http://localhost:3000"},
		AllowCredentials: true,
	}).Handler)

	router.Handle("/", handler.Playground("GraphQL playground", "/graphql"))
	router.Handle("/graphql", handler.GraphQL(
		api.NewExecutableSchema(api.Config{
			Resolvers: &api.Resolver{
				EstimationService: estimationService,
			},
		}),
		handler.WebsocketUpgrader(websocket.Upgrader{
			CheckOrigin: func(r *http.Request) bool {
				return true
			},
		}),
		handler.RecoverFunc(func(ctx context.Context, err interface{}) error {
			fmt.Println(err)
			return fmt.Errorf("Server Error")
		}),
	))

	err := http.ListenAndServe(":8080", router)
	if err != nil {
		fmt.Printf("%v", err)
	}
}
