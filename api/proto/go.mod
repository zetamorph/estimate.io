module github.com/zetamorph/estimate-io/proto

require (
	github.com/go-chi/chi v3.3.2+incompatible // indirect
	github.com/gogo/protobuf v1.2.1
	github.com/golang/protobuf v1.2.0
	github.com/gorilla/context v0.0.0-20160226214623-1ea25387ff6f // indirect
	github.com/gorilla/mux v1.6.1 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/micro/go-micro v0.26.1
	github.com/micro/go-rcache v0.2.1 // indirect
	github.com/opentracing/basictracer-go v1.0.0 // indirect
	github.com/opentracing/opentracing-go v1.0.2 // indirect
	github.com/rs/cors v1.6.0 // indirect
	github.com/shurcooL/httpfs v0.0.0-20171119174359-809beceb2371 // indirect
	github.com/shurcooL/vfsgen v0.0.0-20180121065927-ffb13db8def0 // indirect
	github.com/vektah/dataloaden v0.2.0 // indirect
	github.com/vektah/gqlparser v1.1.0 // indirect
	sourcegraph.com/sourcegraph/appdash v0.0.0-20180110180208-2cc67fd64755 // indirect
	sourcegraph.com/sourcegraph/appdash-data v0.0.0-20151005221446-73f23eafcf67 // indirect
)
