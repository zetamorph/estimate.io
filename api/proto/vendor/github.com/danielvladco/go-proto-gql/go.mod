module github.com/danielvladco/go-proto-gql

require (
	github.com/99designs/gqlgen next
	github.com/agnivade/levenshtein v1.0.1 // indirect
	github.com/gogo/protobuf v1.2.0
	github.com/golang/protobuf v1.2.0
	github.com/gorilla/websocket v1.4.0 // indirect
	github.com/hashicorp/golang-lru v0.5.0 // indirect
	github.com/mwitkow/go-proto-validators v0.0.0-20180403085117-0950a7990007
	github.com/pkg/errors v0.8.1 // indirect
	github.com/urfave/cli v1.20.0 // indirect
	github.com/vektah/gqlparser v1.0.0
	golang.org/x/net v0.0.0-20180826012351-8a410e7b638d
	golang.org/x/tools v0.0.0-20190118193359-16909d206f00 // indirect
	google.golang.org/grpc v1.18.0
	gopkg.in/yaml.v2 v2.2.2
)
