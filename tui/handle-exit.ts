import { MUTATIONS, QUERIES } from '@flyacts/estimate-ts-shared';
import { client } from './client';

export const registerExitHandler = () => {

    const leaveSession = async () => {
        const { data: { currentSession } } = await client.query({
            query: QUERIES.GET_CURRENT_SESSION,
        });
        const { data: { currentParticipant } } = await client.query({
            query: QUERIES.GET_CURRENT_PARTICIPANT,
        });

        await client.mutate({
            mutation: MUTATIONS.LEAVE_SESSION,
            variables: {
                participantId: currentParticipant.id,
                sessionId: currentSession.id,
            },
        });
    };

    process.on('SIGINT', async () => {
        await leaveSession();
        process.exit();
    });

    process.on('SIGTERM', async () => {
        await leaveSession();
        process.exit();
    });
};
