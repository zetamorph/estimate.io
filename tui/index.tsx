import { MUTATIONS, UPDATE_CURRENT_PARTICIPANT_AND_SESSION, QUERIES } from '@flyacts/estimate-ts-shared';
import program from 'commander';
import { Box, render } from 'ink';
import React from 'react';
import { ApolloProvider, Mutation, MutationFn } from 'react-apollo';
import { client } from './client';
import { ParticipantList, TaskList } from './components';
import { registerExitHandler } from './handle-exit';
import { Header } from './components/Header';

process.env.FORCE_COLOR = '1';

interface Props {
    joinSession: MutationFn<any, any>;
    sessionId: string;
}

interface State {
    count: number;
}

class App extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        registerExitHandler();
    }

    public componentWillMount() {
        this.props
        .joinSession()
        .catch(console.log);
    }

    public render() {
        const { sessionId } = this.props;
        return (
            <>
                <Box flexDirection="column">
                    <ParticipantList sessionId={sessionId}></ParticipantList>
                    <TaskList sessionId={sessionId}></TaskList>
                </Box>
            </>
        ); 
    }
}

program
.command('join <sessionId>')
.option('-n, --name <name>', 'Your name')
.action((sessionId, options) => {
    console.clear();
    render(
        <ApolloProvider client={client}>
            <Mutation
                mutation={MUTATIONS.JOIN_SESSION}
                update={UPDATE_CURRENT_PARTICIPANT_AND_SESSION('estimationServiceJoinSession')}
                variables={{
                    participantName: options.name,
                    sessionId,
                }}
            >{(joinSession) =>
                <App
                    joinSession={joinSession}
                    sessionId={sessionId}
                />
            }</Mutation>
        </ApolloProvider>,
    );
});

program.parse(process.argv);
