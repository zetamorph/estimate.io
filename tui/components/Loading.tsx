import { Color } from 'ink';
import React from 'react';

export const Loading = () => <Color red>Loading...</Color>;
