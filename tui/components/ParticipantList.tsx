import { Participant, ParticipantListContainer } from '@flyacts/estimate-ts-shared';
import { Box, Color } from 'ink';
import React from 'react';
import { Loading } from './Loading';

export interface ParticipantListProps {
    sessionId: string;
}

export const ParticipantList = ({ sessionId }: ParticipantListProps) => (
    <Box justifyContent="space-around">
        <ParticipantListContainer
            sessionId={sessionId}
            loading={<Loading />}
        >{(participants: Participant[], currentParticipant: Participant) => <>{
            participants.map((participant) => (
                <Box
                    key={participant.id}
                    marginY={1}
                    marginX={2}
                >
                    <Color
                        red={participant.id === currentParticipant.id}
                        yellow={participant.isManager}
                    >{participant.name}</Color>
                </Box>
            ))
        }</>}</ParticipantListContainer>
    </Box>
);
