import { QUERIES } from '@flyacts/estimate-ts-shared';
import { Text } from 'ink';
import Box from 'ink-box';
import Gradient from 'ink-gradient';
import React from 'react';
import { Query } from 'react-apollo';
import { randomGradient } from '../util';
import { Loading } from './Loading';

export const Header = () => <>
    <Query query={QUERIES.GET_CURRENT_SESSION}>{({ data, loading }) => {
        if (loading || !data.currentSession.name) {
            return <Loading />;
        }
        return (
            <Box borderStyle="round" float="center" marginY={1}>
                <Gradient name={randomGradient}>
                    <Text
                        underline={true}
                        bold={true}
                    >{data.currentSession.name}</Text>
                </Gradient>
            </Box>
        );
    }}</Query>
</>;
