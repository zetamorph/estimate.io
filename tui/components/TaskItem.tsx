import { Task, TaskState } from '@flyacts/estimate-ts-shared';
import { Box, Color } from 'ink';
import React from 'react';
import { EstimationList } from './EstimationList';

interface Props {
    task: Task;
}

export const TaskItem = ({ task }: Props) => {
    const isActive = task.state === TaskState.ACTIVE;
    console.log({ isActive });
    return (
        <Box key={task.id} flexDirection="row">
            <Color
                bgBlackBright={isActive}
                green
            >{task.name}</Color>
            {isActive && <EstimationList task={task}></EstimationList>}
        </Box>
    );
};
