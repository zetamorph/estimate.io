import { Estimate, EstimationListContainer, EstimationListProps, MUTATIONS, Task } from '@flyacts/estimate-ts-shared';
import { Box, Color, Text } from 'ink';
import SelectInput from 'ink-select-input';
import React from 'react';
import { Mutation } from 'react-apollo';
import { Loading } from './Loading';

interface Props {
    task: Task;
}

export const EstimationList = ({ task }: Props) => {
    const selectOptions = Object.values<number>(Estimate as any)
        .filter((value) => typeof value === 'number')
        .map((estimate) => ({ value: estimate, label: String(estimate), key: estimate }));

    return (
        <Box flexDirection="row">
            <EstimationListContainer
                loading={<Loading />}
                task={task}
            >{({ estimations, estimatesVisible, participantId }: EstimationListProps) => estimatesVisible
                ? estimations.map((estimation) =>
                    <Box flexDirection="row" alignItems="flex-end" key={estimation.id}>
                        <Color yellowBright key={estimation.id} >
                            <Box marginX={2} marginY={1}>
                                <Text>{estimation.estimate}</Text>
                            </Box>
                        </Color>
                    </Box>,
                )
                : (
                    <Mutation
                        mutation={MUTATIONS.ADD_ESTIMATE}
                    >{(addEstimate) =>
                        <SelectInput items={selectOptions} onSelect={(item: { value: number }) => {
                            addEstimate({ variables: {
                                participantId,
                                taskId: task.id,
                                value: item.value,
                            }});
                        }}></SelectInput>
                    }</Mutation>
                )
            }</EstimationListContainer>
        </Box>
    );
};
