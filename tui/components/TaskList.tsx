import { TaskListContainer, TaskState } from '@flyacts/estimate-ts-shared';
import { Box, Color } from 'ink';
import React from 'react';
import { EstimationList } from './EstimationList';
import { Loading } from './Loading';
import { TaskItem } from './TaskItem';

interface Props {
    sessionId: string;
}

export const TaskList = ({ sessionId }: Props) => {
    return (
        <Box flexDirection="column" margin={1}>
            <TaskListContainer
                sessionId={sessionId}
                loading={<Loading />}
            >{(tasks) =>
                tasks.map((task) => <TaskItem key={task.id} task={task}/>)
            }
            </TaskListContainer>
        </Box>
    );
};
