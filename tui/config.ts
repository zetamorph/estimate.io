import { cleanEnv, url } from 'envalid';

export const config = cleanEnv(process.env, {
    GRAPHQL_HTTP_URL: url(),
    GRAPHQL_WS_URL: url(),
});
