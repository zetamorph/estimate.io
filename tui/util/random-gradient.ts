const getRandomGradient = () => {
    const gradients = [
        'cristal',
        'teen',
        'mind',
        'morning',
        'vice',
        'passion',
        'fruit',
        'instagram',
        'atlas',
        'retro',
        'summer',
        'pastel',
    ] as const;

    const index = Math.floor(Math.random() * gradients.length);
    return gradients[index];
};

export const randomGradient = getRandomGradient();
