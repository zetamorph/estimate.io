import { defaults, Participant, resolvers, Session } from '@flyacts/estimate-ts-shared';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloClient } from 'apollo-client';
import { from, split } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { withClientState } from 'apollo-link-state';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';
import { OperationDefinitionNode } from 'graphql';
import ws from 'isomorphic-ws';
import fetch from 'node-fetch';
import { config } from './config';

const httpLink = new HttpLink({
  fetch: fetch as any,
  uri: config.GRAPHQL_HTTP_URL,
});

const wsLink = new WebSocketLink({
    options: {
        reconnect: true,
        timeout: 30000,
    },
    uri: config.GRAPHQL_WS_URL,
    webSocketImpl: ws,
});

const cache = new InMemoryCache();

const stateLink = withClientState({
  cache,
  defaults,
  resolvers,
});

const remoteLink = split(
  // split based on operation type
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query) as OperationDefinitionNode;
    return kind === 'OperationDefinition' && operation === 'subscription';
  },
  wsLink,
  httpLink,
);

export const client = new ApolloClient({
  cache,
  link: from([
    stateLink,
    remoteLink,
  ]),
});

export interface ClientState {
  currentSession: Session | null;
  currentParticipant: Participant | null;
  clientId: string;
}
