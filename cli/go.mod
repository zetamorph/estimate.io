module github.com/zetamorph/estimate-io-client

require (
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v1.3.1
	github.com/zetamorph/estimate-io-client/cmd v0.0.0
)

replace github.com/zetamorph/estimate-io-client/cmd => ./cmd
