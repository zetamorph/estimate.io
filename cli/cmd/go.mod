module github.com/zetamorph/estimate-io-client/cmd

require (
	github.com/boltdb/bolt v1.3.1
	github.com/google/uuid v1.1.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v1.3.1
	golang.org/x/arch v0.0.0-20181203225421-5a4828bb7045 // indirect
)
