package cmd

import (
	"fmt"

	"encoding/json"

	"github.com/boltdb/bolt"
	"github.com/google/uuid"
	"github.com/spf13/cobra"
)

type Session struct {
	Id string
}

func init() {
	rootCmd.AddCommand(sessionCmd)
}

var sessionCmd = &cobra.Command{
	Use:   "session start [name]",
	Short: "Start a new session",
	Long:  `Start a new session`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("%v", args)
		db, err := bolt.Open("my.db", 0600, nil)
		if err != nil {
			panic(err)
		}
		defer db.Close()
		db.Update(func(tx *bolt.Tx) error {
			b, err := tx.CreateBucket([]byte("Sessions"))
			if err != nil {
				return fmt.Errorf("create bucket: %s", err)
			}
			id := uuid.New()
			encoded, err := json.Marshal(Session{
				Id: id.String(),
			})
			if err != nil {
				fmt.Println("Error while marshaling session to json")
			}
			err = b.Put([]byte(args[0]), encoded)
			if err != nil {
				fmt.Println(err)
			}
			return nil
		})
	},
}
