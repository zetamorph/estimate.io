package main

import (
	context "context"
	"errors"
	"fmt"
	"os"

	"database/sql"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	micro "github.com/micro/go-micro"
	viper "github.com/spf13/viper"
	estimate "github.com/zetamorph/estimate-io/estimation/estimate"
	participant "github.com/zetamorph/estimate-io/estimation/participant"
	session "github.com/zetamorph/estimate-io/estimation/session"
	task "github.com/zetamorph/estimate-io/estimation/task"
	util "github.com/zetamorph/estimate-io/estimation/util"
	proto "github.com/zetamorph/estimate-io/proto"
)

type repository struct {
	session     session.Repository
	task        task.Repository
	participant participant.Repository
	estimate    estimate.Repository
}

// EstimationService struct
type EstimationService struct {
	repo *repository
}

// Init sets defaults for environment variables
func Init() {
	viper.SetDefault("DB_HOST", "localhost")
	viper.SetDefault("DB_PORT", "5432")
	viper.SetDefault("DB_PASSWORD", "123456")
	viper.SetDefault("DB_NAME", "estimations")
	viper.SetDefault("DB_USER", "postgres")
	viper.AutomaticEnv()
}

func main() {
	Init()
	dbConnStr := fmt.Sprintf(
		"user=%s password=%s port=%s dbname=%s host=%s sslmode=disable",
		viper.Get("DB_USER"),
		viper.Get("DB_PASSWORD"),
		viper.Get("DB_PORT"),
		viper.Get("DB_NAME"),
		viper.Get("DB_HOST"),
	)
	db, err := sqlx.Open("postgres", dbConnStr)
	handleError(err)
	service := micro.NewService(micro.Name("estimation"))
	service.Init()

	// Register handler
	proto.RegisterEstimationServiceHandler(service.Server(), &EstimationService{
		repo: &repository{
			session:     session.NewPgRepository(db),
			task:        task.NewPgRepository(db, dbConnStr),
			participant: participant.NewPgRepository(db, dbConnStr),
			estimate:    estimate.NewPgRepository(db, dbConnStr),
		},
	})

	// Run the server
	if err := service.Run(); err != nil {
		handleError(err)
	}
}

func handleError(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

// CreateSession Handler
func (e *EstimationService) CreateSession(ctx context.Context, req *proto.CreateSessionRequest, rsp *proto.CreateSessionResponse) error {
	createdSession, err := e.repo.session.Create(&session.Session{
		Name:        req.Name,
		Timebox:     *util.ProtoInt64ToNullInt64(req.Timebox),
		TaskTimebox: *util.ProtoInt64ToNullInt64(req.TaskTimebox),
	})
	if err != nil {
		return err
	}
	session, err := createdSession.ToProto()
	if err != nil {
		return err
	}
	rsp.Session = session
	createdParticipant, err := e.repo.participant.Create(&participant.Participant{
		Name:      req.ManagerName,
		SessionID: createdSession.ID,
		IsManager: true,
	})
	if err != nil {
		return err
	}
	rsp.Participant = createdParticipant.ToProto()
	return nil
}

// GetSession handler
func (e *EstimationService) GetSession(ctx context.Context, req *proto.GetSessionRequest, rsp *proto.GetSessionResponse) error {
	foundSession, err := e.repo.session.FindByID(req.Id)
	if err != nil {
		return err
	}
	session, err := foundSession.ToProto()
	if err != nil {
		return err
	}
	rsp.Session = session
	return nil
}

// AddTask handler
func (e *EstimationService) AddTask(ctx context.Context, req *proto.AddTaskRequest, rsp *proto.AddTaskResponse) error {
	createdTask, err := e.repo.task.Create(&task.Task{Name: req.Name, SessionID: req.SessionId})
	if err != nil {
		return err
	}
	fmt.Printf("%v\n", createdTask)
	rsp.Task = createdTask.ToProto()
	return nil
}

// ActivateTask handler
func (e *EstimationService) ActivateTask(ctx context.Context, req *proto.ActivateTaskRequest, rsp *proto.ActivateTaskResponse) error {
	current, err := e.repo.task.FindByID(req.TaskId)
	if err != nil {
		return err
	}
	if current.State != "inactive" {
		return errors.New("Task can´t be activated")
	}
	updatedTask, err := e.repo.task.UpdateByID(req.TaskId, &task.Task{
		Name:  current.Name,
		State: "active",
	})
	if err != nil {
		return err
	}
	fmt.Printf("%v\n", updatedTask)
	return nil
}

// DeleteTask handler
func (e *EstimationService) DeleteTask(ctx context.Context, req *proto.DeleteTaskRequest, rsp *proto.DeleteTaskResponse) error {
	err := e.repo.task.DeleteByID(req.TaskId)
	if err != nil {
		return err
	}
	return nil
}

// GetTasks handler
func (e *EstimationService) GetTasks(ctx context.Context, req *proto.GetTasksRequest, rsp *proto.GetTasksResponse) error {
	var tasks []*proto.Task
	foundTasks, err := e.repo.task.FindBySessionID(req.SessionId)
	if err != nil {
		return err
	}
	for _, task := range foundTasks {
		tasks = append(tasks, task.ToProto())
	}
	rsp.Tasks = tasks
	return nil
}

// GetEstimates handler
func (e *EstimationService) GetEstimates(ctx context.Context, req *proto.GetEstimatesRequest, rsp *proto.GetEstimatesResponse) error {
	var estimates []*proto.Estimate
	foundEstimates, err := e.repo.estimate.FindByTaskID(req.TaskId)
	if err != nil {
		return err
	}
	for _, estimate := range foundEstimates {
		estimates = append(estimates, estimate.ToProto())
	}
	rsp.Estimates = estimates
	return nil
}

// GetParticipants handler
func (e *EstimationService) GetParticipants(ctx context.Context, req *proto.GetParticipantsRequest, rsp *proto.GetParticipantsResponse) error {
	var participants []*proto.Participant
	foundParticipants, err := e.repo.participant.FindBySessionID(req.SessionId)
	if err != nil {
		return err
	}
	for _, participant := range foundParticipants {
		participants = append(participants, participant.ToProto())
	}
	rsp.Participants = participants
	return nil
}

// JoinSession handler
func (e *EstimationService) JoinSession(ctx context.Context, req *proto.JoinSessionRequest, rsp *proto.JoinSessionResponse) error {
	foundSession, err := e.repo.session.FindByID(req.SessionId)
	if err != nil {
		return err
	}
	participant, err := e.repo.participant.Create(&participant.Participant{
		Name:      req.ParticipantName,
		SessionID: req.SessionId,
	})
	if err != nil {
		return err
	}
	session, err := foundSession.ToProto()
	if err != nil {
		return err
	}
	rsp.Participant = participant.ToProto()
	rsp.Session = session
	return nil
}

// LeaveSession handler
func (e *EstimationService) LeaveSession(ctx context.Context, req *proto.LeaveSessionRequest, rsp *proto.LeaveSessionResponse) error {
	err := e.repo.participant.DeleteByID(req.ParticipantId)
	return err
}

// ListenParticipants server->client stream handler
func (e *EstimationService) ListenParticipants(ctx context.Context, req *proto.GetParticipantsRequest, stream proto.EstimationService_ListenParticipantsStream) error {
	out := make(chan []*participant.Participant)
	err := e.repo.participant.ListenBySessionID(req.SessionId, out)
	if err != nil {
		return err
	}
	for {
		participants := <-out
		var streamData []*proto.Participant
		for _, participant := range participants {
			streamData = append(streamData, participant.ToProto())
		}
		err = stream.SendMsg(&proto.GetParticipantsResponse{
			Participants: streamData,
		})
		if err != nil {
			return err
		}
	}
}

// ListenTasks handler
func (e *EstimationService) ListenTasks(ctx context.Context, req *proto.GetTasksRequest, stream proto.EstimationService_ListenTasksStream) error {
	out := make(chan []*task.Task)
	err := e.repo.task.ListenBySessionID(req.SessionId, out)
	if err != nil {
		return err
	}
	for {
		tasks := <-out
		var streamTasks []*proto.Task
		for _, task := range tasks {
			streamTasks = append(streamTasks, task.ToProto())
		}
		err = stream.SendMsg(&proto.GetTasksResponse{
			Tasks: streamTasks,
		})
		if err != nil {
			return err
		}
	}
}

// ListenEstimates handler
func (e *EstimationService) ListenEstimates(ctx context.Context, req *proto.GetEstimatesRequest, stream proto.EstimationService_ListenEstimatesStream) error {
	out := make(chan []*estimate.Estimate)
	err := e.repo.estimate.ListenByTaskID(req.TaskId, out)
	if err != nil {
		return err
	}
	for {
		estimates := <-out
		var streamData []*proto.Estimate
		for _, estimate := range estimates {
			streamData = append(streamData, estimate.ToProto())
		}
		err = stream.SendMsg(&proto.GetEstimatesResponse{
			Estimates: streamData,
		})
		if err != nil {
			return err
		}
	}
}

// AddEstimate handler
func (e *EstimationService) AddEstimate(ctx context.Context, req *proto.AddEstimateRequest, rsp *proto.AddEstimateResponse) error {
	createdEstimate, err := e.repo.estimate.Create(&estimate.Estimate{
		TaskID:        req.Estimate.TaskId,
		ParticipantID: req.Estimate.ParticipantId,
		Estimate:      req.Estimate.Estimate,
	})
	if err != nil {
		return err
	}
	rsp.Estimate = createdEstimate.ToProto()
	return nil
}

// RevealEstimates handler
func (e *EstimationService) RevealEstimates(ctx context.Context, req *proto.RevealEstimatesRequest, rsp *proto.RevealEstimatesResponse) error {
	task, err := e.repo.task.FindByID(req.TaskId)
	if err != nil {
		return err
	}
	task.EstimatesVisible = true
	_, err = e.repo.task.UpdateByID(req.TaskId, task)
	if err != nil {
		return err
	}
	return nil
}

// ResetTask handler
func (e *EstimationService) ResetTask(ctx context.Context, req *proto.ResetTaskRequest, rsp *proto.ResetTaskResponse) error {
	task, err := e.repo.task.FindByID(req.TaskId)
	if err != nil {
		return err
	}
	if task.State == "complete" {
		return errors.New("Task is already complete")
	}
	task.EstimatesVisible = false
	_, err = e.repo.task.UpdateByID(req.TaskId, task)
	err = e.repo.estimate.DeleteByTaskID(req.TaskId)
	return err
}

// CompleteTask handler
func (e *EstimationService) CompleteTask(ctx context.Context, req *proto.CompleteTaskRequest, rsp *proto.CompleteTaskResponse) error {
	task, err := e.repo.task.FindByID(req.TaskId)
	if err != nil {
		return err
	}
	if task.State == "complete" {
		return errors.New("Task is already complete")
	}
	task.State = "complete"
	estimation := int64(req.Estimation)
	task.Estimation = sql.NullInt64{
		Int64: estimation,
		Valid: true,
	}
	task.EstimatesVisible = true
	_, err = e.repo.task.UpdateByID(req.TaskId, task)
	if err != nil {
		return err
	}
	return nil
}
