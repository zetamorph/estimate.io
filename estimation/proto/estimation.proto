syntax = "proto3";
package proto;
option go_package = "github.com/zetamorph/estimate.io/proto";

import "github.com/danielvladco/go-proto-gql/gql.proto";
import "google/protobuf/timestamp.proto";
import "google/protobuf/wrappers.proto";

service EstimationService {
    rpc CreateSession(CreateSessionRequest) returns (CreateSessionResponse) {
        option (gql.rpc_type) = MUTATION;
    }
    rpc GetSession(GetSessionRequest) returns (GetSessionResponse) {
        option (gql.rpc_type) = QUERY;
    }
    rpc JoinSession(JoinSessionRequest) returns (JoinSessionResponse) {
        option (gql.rpc_type) = MUTATION;
    }
    rpc LeaveSession(LeaveSessionRequest) returns (LeaveSessionResponse) {
        option (gql.rpc_type) = MUTATION;
    }
    rpc AddTask(AddTaskRequest) returns (AddTaskResponse) {
        option (gql.rpc_type) = MUTATION;
    }
    rpc ListenTasks(GetTasksRequest) returns (stream GetTasksResponse) {
        option (gql.rpc_type) = DEFAULT;
    }
    rpc ListenParticipants(GetParticipantsRequest) returns (stream GetParticipantsResponse) {
        option (gql.rpc_type) = DEFAULT;
    }
    rpc ActivateTask(ActivateTaskRequest) returns (ActivateTaskResponse) {
        option (gql.rpc_type) = MUTATION;
    }
    rpc DeleteTask(DeleteTaskRequest) returns (DeleteTaskResponse) {
        option (gql.rpc_type) = MUTATION;
    }
    rpc ListenEstimates(GetEstimatesRequest) returns (stream GetEstimatesResponse) {
        option (gql.rpc_type) = DEFAULT;
    }
    rpc AddEstimate(AddEstimateRequest) returns (AddEstimateResponse) {
        option (gql.rpc_type) = MUTATION;
    }
    rpc ResetTask(ResetTaskRequest) returns (ResetTaskResponse) {
        option (gql.rpc_type) = MUTATION;
    }
    rpc CompleteTask(CompleteTaskRequest) returns (CompleteTaskResponse) {
        option (gql.rpc_type) = MUTATION;
    }
    rpc RevealEstimates(RevealEstimatesRequest) returns (RevealEstimatesResponse) {
        option (gql.rpc_type) = MUTATION;
    }
    rpc GetTasks(GetTasksRequest) returns (GetTasksResponse) {
        option (gql.rpc_type) = QUERY;
    }
    rpc GetEstimates(GetEstimatesRequest) returns (GetEstimatesResponse) {
        option (gql.rpc_type) = QUERY;
    }
    rpc GetParticipants(GetParticipantsRequest) returns (GetParticipantsResponse) {
        option (gql.rpc_type) = QUERY;
    }
}

message RevealEstimatesRequest {
    string task_id = 1;
}

message RevealEstimatesResponse {}

message ResetTaskRequest {
    string task_id = 1;
}

message ResetTaskResponse {}

message CompleteTaskRequest {
    string task_id = 1;
    int32 estimation = 2;
}

message CompleteTaskResponse {}

message Session {
    string id = 1;
    string name = 2;
    google.protobuf.Int64Value timebox = 3;
    google.protobuf.Int64Value task_timebox = 4;
    google.protobuf.Timestamp created_at = 5;
}

enum TaskState {
    INACTIVE = 0;
    ACTIVE = 1;
    COMPLETE = 2;
}

message Task {
    string id = 1;
    string name = 2;
    string session_id = 3;
    TaskState state = 4;
    int32 estimation = 5;
    bool estimates_visible = 6;
}

message Participant {
    string id = 1;
    string name = 2;
    string session_id = 3;
    bool is_manager = 4;
}

message Estimate {
    string id = 1;
    string task_id = 2;
    sint32 estimate = 3;
    string participant_id = 4;
}

message AddEstimateRequest {
    Estimate estimate = 1;
}

message AddEstimateResponse {
    Estimate estimate = 1;
}

message GetEstimatesRequest {
    string task_id = 1;
}

message GetEstimatesResponse {
    repeated Estimate estimates = 1;
}

message GetParticipantsRequest {
    string session_id = 1;
}

message GetParticipantsResponse {
    repeated Participant participants = 1;
}

message JoinSessionRequest {
    string session_id = 1;
    string participant_name = 2;
}

message JoinSessionResponse {
    Participant participant = 1;
    Session session = 2;
}

message LeaveSessionRequest {
    string session_id = 1;
    string participant_id = 2;
}

message LeaveSessionResponse {}

message CreateSessionRequest {
    string name = 1;
    string manager_name = 2;
    google.protobuf.Int64Value timebox = 3;
    google.protobuf.Int64Value task_timebox = 4;
}

message CreateSessionResponse {
    Session session = 1;
    Participant participant = 2;
}

message GetSessionRequest {
    string id = 1;
}

message GetSessionResponse {
    Session session = 1;
}

message AddTaskRequest {
    string name = 1;
    string session_id = 2;
}

message AddTaskResponse {
    Task task = 1;
}

message DeleteTaskRequest {
    string task_id = 1;
}

message DeleteTaskResponse {}

message ActivateTaskRequest {
    string task_id = 1;
}

message ActivateTaskResponse {}

message GetTasksRequest {
    string session_id = 1;
}

message GetTasksResponse {
    repeated Task tasks = 1;
}
