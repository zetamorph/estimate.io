module github.com/zetamorph/estimate-io/estimation/task

require (
	github.com/danielvladco/go-proto-gql v0.2.0 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.0.0
	github.com/zetamorph/estimate-io/estimation/pgbroadcaster v0.0.0
	github.com/zetamorph/estimate-io/proto v0.0.0
)

replace github.com/zetamorph/estimate-io/estimation/pgbroadcaster => ../pgbroadcaster

replace github.com/zetamorph/estimate-io/proto => ../../proto
