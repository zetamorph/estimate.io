package task

// Repository for Tasks
type Repository interface {
	Create(task *Task) (*Task, error)
	FindBySessionID(sessionID string) ([]*Task, error)
	FindByID(ID string) (*Task, error)
	ListenBySessionID(sessionID string, taskChanges chan []*Task) error
	UpdateByID(ID string, task *Task) (*Task, error)
	UpdateBySessionID(sessionID, col string, value interface{}) error
	DeleteByID(ID string) error
}
