package task

import (
	"database/sql"
	"strings"
	"time"

	proto "github.com/zetamorph/estimate-io/proto"
)

// Task struct
type Task struct {
	ID               string        `db:"id"`
	Name             string        `db:"name"`
	SessionID        string        `db:"session_id"`
	State            string        `db:"state"`
	Estimation       sql.NullInt64 `db:"estimation"`
	EstimatesVisible bool          `db:"estimates_visible"`
	CreatedAt        time.Time     `db:"created_at"`
	ActivatedAt      time.Time     `db:"activated_at"`
}

// ToProto converts a Task struct to a go-grpc struct
func (t *Task) ToProto() *proto.Task {
	estimation := int32(t.Estimation.Int64)
	return &proto.Task{
		Name:             t.Name,
		Id:               t.ID,
		SessionId:        t.SessionID,
		State:            proto.TaskState(proto.TaskState_value[strings.ToUpper(t.State)]),
		Estimation:       estimation,
		EstimatesVisible: t.EstimatesVisible,
	}
}
