package task

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	"github.com/jmoiron/sqlx"

	bc "github.com/zetamorph/estimate-io/estimation/pgbroadcaster"

	pq "github.com/lib/pq"
)

type repo struct {
	db          *sqlx.DB
	broadcaster *bc.PgBroadcaster
}

// NewPgRepository - creates a new Postgres repo
func NewPgRepository(db *sqlx.DB, connStr string) Repository {
	reportProblem := func(ev pq.ListenerEventType, err error) {
		if err != nil {
			fmt.Println(err.Error())
		}
	}

	listener := pq.NewListener(connStr, 10*time.Second, time.Minute, reportProblem)
	err := listener.Listen("tasks_change")
	if err != nil {
		panic(err)
	}

	broadcaster := bc.NewPgBroadcaster()
	broadcaster.Init(listener, "SessionID")

	return &repo{
		db,
		broadcaster,
	}
}

func (r *repo) UpdateByID(ID string, task *Task) (*Task, error) {
	rows, err := r.db.Query(`
		UPDATE tasks
		SET (name, state, estimation, estimates_visible) = ($2, $3, $4, $5)
		WHERE id = $1
		RETURNING id, name, session_id, state, estimation, estimates_visible
	`, ID, task.Name, task.State, task.Estimation, task.EstimatesVisible)
	defer rows.Close()
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	var updatedTask Task
	for rows.Next() {
		if err := rows.Scan(
			&updatedTask.ID,
			&updatedTask.Name,
			&updatedTask.SessionID,
			&updatedTask.State,
			&updatedTask.Estimation,
			&updatedTask.EstimatesVisible,
		); err != nil {
			fmt.Println(err)
			return nil, err
		}
	}
	return &updatedTask, nil
}

func (r *repo) UpdateBySessionID(sessionID string, col string, value interface{}) error {
	_, err := r.db.Exec(`
		UPDATE tasks
		SET $2 = $3
		WHERE session_id = $1
	`, sessionID, col, value)
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

func (r *repo) DeleteByID(ID string) error {
	_, err := r.db.Exec(`
		DELETE FROM tasks
		WHERE id = $1
	`, ID)
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

func (r *repo) FindByID(ID string) (*Task, error) {
	var foundTask Task
	row := r.db.QueryRow(`
		SELECT id, name, session_id, state
		FROM tasks
		WHERE id = $1
	`, ID)
	err := row.Scan(&foundTask.ID, &foundTask.Name, &foundTask.SessionID, &foundTask.State)
	switch err {
	case sql.ErrNoRows:
		return nil, err
	case nil:
		return &foundTask, nil
	default:
		log.Fatal(err)
		return nil, err
	}
}

func (r *repo) Create(task *Task) (*Task, error) {
	rows, err := r.db.Query(`
		INSERT INTO tasks (name, session_id)
		VALUES ($1, $2)
		RETURNING id, name, session_id, state
	`, task.Name, task.SessionID)
	defer rows.Close()
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	var createdTask Task
	for rows.Next() {
		if err := rows.Scan(&createdTask.ID, &createdTask.Name, &createdTask.SessionID, &createdTask.State); err != nil {
			fmt.Println(err)
			return nil, err
		}
	}
	return &createdTask, nil
}

func (r *repo) FindBySessionID(sessionID string) ([]*Task, error) {
	var foundTasks []*Task
	rows, err := r.db.Query(`
		SELECT 
			id,
			name,
			session_id,
			state, 
			estimation,
			estimates_visible
		FROM tasks
		WHERE session_id = $1
		ORDER BY state
	`, sessionID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		task := &Task{}
		if err := rows.Scan(
			&task.ID,
			&task.Name,
			&task.SessionID,
			&task.State,
			&task.Estimation,
			&task.EstimatesVisible,
		); err != nil {
			return nil, err
		}
		foundTasks = append(foundTasks, task)
	}
	return foundTasks, nil
}

func (r *repo) ListenBySessionID(sessionID string, taskChanges chan []*Task) error {
	s := bc.NewSubscriber()
	r.broadcaster.AddSubscriber(s)
	go func() {
		initialTasks, err := r.FindBySessionID(sessionID)
		if err != nil {
			fmt.Println(err)
			return
		}
		taskChanges <- initialTasks
		for {
			changedSessionID := <-s.Changes
			if changedSessionID == sessionID {
				tasks, err := r.FindBySessionID(sessionID)
				if err != nil {
					fmt.Println(err)
					return
				}
				taskChanges <- tasks
			}
		}
	}()
	return nil
}
