package pgbroadcaster

import (
	"encoding/json"
	"log"
	"sync"
	"time"

	pq "github.com/lib/pq"
)

// PgBroadcaster can broadcast pg events
type PgBroadcaster struct {
	sync.Mutex
	subscribers []*Subscriber
	recv        chan string
}

type changes struct {
	Data changesData `json:"data"`
}

type changesData struct {
	SessionID string `json:"session_id"`
	TaskID    string `json:"task_id"`
}

// AddSubscriber registers a subscriber
func (b *PgBroadcaster) AddSubscriber(s *Subscriber) {
	b.Lock()
	defer b.Unlock()
	b.subscribers = append(b.subscribers, s)
}

// Iter iterates over the broadcaster´s subscribers
func (b *PgBroadcaster) Iter(routine func(*Subscriber)) {
	b.Lock()
	defer b.Unlock()

	for _, s := range b.subscribers {
		routine(s)
	}
}

// Subscriber can subscribe to a broadcaster
type Subscriber struct {
	Changes chan string
	quit    chan struct{}
}

// NewSubscriber Returns a new subscriber
func NewSubscriber() *Subscriber {
	return &Subscriber{
		Changes: make(chan string, 10),
	}
}

// NewPgBroadcaster Returns a new boradcaster
func NewPgBroadcaster() *PgBroadcaster {
	return &PgBroadcaster{
		subscribers: make([]*Subscriber, 0),
		recv:        make(chan string),
	}
}

// Init a new broadcaster
func (b *PgBroadcaster) Init(source *pq.Listener, notifyBy string) {
	// listener
	go func() {
		for {
			select {
			case notification := <-source.Notify:
				data := changes{}
				err := json.Unmarshal([]byte(notification.Extra), &data)
				if err != nil {
					log.Fatal(err)
				}
				if notifyBy == "SessionID" {
					b.recv <- data.Data.SessionID
				}
				if notifyBy == "TaskID" {
					b.recv <- data.Data.TaskID
				}
			case <-time.After(60 * time.Second):
				// check if the connection is still active
				go source.Ping()
			}
		}
	}()

	// dispatcher
	go func() {
		for {
			msg := <-b.recv
			b.Iter(func(s *Subscriber) { s.Changes <- msg })
		}
	}()
}
