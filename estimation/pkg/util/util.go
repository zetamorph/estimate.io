package util

import (
	"database/sql"

	types "github.com/gogo/protobuf/types"
)

// ProtoInt64ToNullInt64 converts a protobuf Int64Value wrapper to sql.NullInt64
func ProtoInt64ToNullInt64(i *types.Int64Value) *sql.NullInt64 {
	if i != nil {
		return &sql.NullInt64{
			Valid: true,
			Int64: i.Value,
		}
	}
	return &sql.NullInt64{
		Valid: false,
	}
}

// NullInt64ToProtoInt64 converts an sql.NullInt64int64 wrapper to a protobuf Int64Value wrapper
func NullInt64ToProtoInt64(i *sql.NullInt64) *types.Int64Value {
	if i.Valid {
		return &types.Int64Value{
			Value: i.Int64,
		}
	}
	return nil
}
