module github.com/zetamorph/estimate-io/estimation/participant

require (
	github.com/danielvladco/go-proto-gql v0.2.0 // indirect
	github.com/google/pprof v0.0.0-20190208070709-b421f19a5c07 // indirect
	github.com/ianlancetaylor/demangle v0.0.0-20181102032728-5e5cf60278f6 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.0.0
	github.com/zetamorph/estimate-io/proto v0.0.0
	golang.org/x/arch v0.0.0-20181203225421-5a4828bb7045 // indirect
	golang.org/x/sys v0.0.0-20190219092855-153ac476189d // indirect
)

replace github.com/zetamorph/estimate-io/proto => ../../proto
