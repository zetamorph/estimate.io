package participant

// Repository for Participant
type Repository interface {
	Create(participant *Participant) (*Participant, error)
	FindBySessionID(sessionID string) ([]*Participant, error)
	ListenBySessionID(sessionID string, out chan []*Participant) error
	CountBySessionID(sessionID string) (*int32, error)
	DeleteByID(ID string) error
}
