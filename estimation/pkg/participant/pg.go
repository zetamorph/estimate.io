package participant

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"sync"
	"time"

	"github.com/jmoiron/sqlx"

	"github.com/lib/pq"
)

type threadSafeSlice struct {
	sync.Mutex
	subscribers []*participantSubscriber
}

func (slice *threadSafeSlice) Push(w *participantSubscriber) {
	slice.Lock()
	defer slice.Unlock()

	slice.subscribers = append(slice.subscribers, w)
}

func (slice *threadSafeSlice) Iter(routine func(*participantSubscriber)) {
	slice.Lock()
	defer slice.Unlock()

	for _, worker := range slice.subscribers {
		routine(worker)
	}
}

type participantSubscriber struct {
	changes chan string
	quit    chan struct{}
}

func (s *participantSubscriber) Start() {
	s.changes = make(chan string, 10)
}

type participantChange struct {
	Data participantData `json:"data"`
}

type participantData struct {
	SessionID string `json:"session_id"`
}

type repo struct {
	db          *sqlx.DB
	listener    *pq.Listener
	subscribers *threadSafeSlice
}

// NewPgRepository - creates a new Postgres repo
func NewPgRepository(db *sqlx.DB, connStr string) Repository {
	reportProblem := func(ev pq.ListenerEventType, err error) {
		if err != nil {
			fmt.Println(err.Error())
		}
	}

	listener := pq.NewListener(connStr, 10*time.Second, time.Minute, reportProblem)
	err := listener.Listen("participants_change")
	if err != nil {
		panic(err)
	}

	subscriberSlice := &threadSafeSlice{
		subscribers: make([]*participantSubscriber, 0),
	}

	changesBroadcast := make(chan string)

	// listener
	go func() {
		for {
			select {
			case notification := <-listener.Notify:
				data := participantChange{}
				err := json.Unmarshal([]byte(notification.Extra), &data)
				if err != nil {
					log.Fatal(err)
				}
				fmt.Printf("Participant change for session %s\n", data.Data.SessionID)
				changesBroadcast <- data.Data.SessionID
			case <-time.After(60 * time.Second):
				// check if the connection is still active
				go listener.Ping()
			}
		}
	}()
	// dispatcher
	go func() {
		for {
			msg := <-changesBroadcast
			subscriberSlice.Iter(func(p *participantSubscriber) { p.changes <- msg })
		}
	}()

	return &repo{
		db,
		listener,
		subscriberSlice,
	}
}

// CountBySessionID Participants
func (r *repo) CountBySessionID(sessionID string) (*int32, error) {
	row := r.db.QueryRow(`
		SELECT COUNT(id)
		FROM participants
		WHERE session_id = $1
	`, sessionID)
	var count *int32
	err := row.Scan(&count)
	switch err {
	case sql.ErrNoRows:
		return nil, err
	case nil:
		return count, nil
	default:
		log.Fatal(err)
		return nil, err
	}
}

// Create an Participant
func (r *repo) Create(participant *Participant) (*Participant, error) {
	rows, err := r.db.Query(`
		INSERT INTO participants (name, session_id, is_manager)
		VALUES ($1, $2, $3)
		RETURNING id, name, session_id, is_manager
	`, participant.Name, participant.SessionID, participant.IsManager)
	defer rows.Close()
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	var createdParticipant Participant
	for rows.Next() {
		if err := rows.Scan(
			&createdParticipant.ID,
			&createdParticipant.Name,
			&createdParticipant.SessionID,
			&createdParticipant.IsManager,
		); err != nil {
			return nil, err
		}
	}
	return &createdParticipant, nil
}

func (r *repo) FindBySessionID(sessionID string) ([]*Participant, error) {
	var foundParticipants []*Participant
	rows, err := r.db.Query(`
		SELECT id, name, session_id, is_manager
		FROM participants
		WHERE session_id = $1
	`, sessionID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		participant := &Participant{}
		err := rows.Scan(
			&participant.ID,
			&participant.Name,
			&participant.SessionID,
			&participant.IsManager,
		)
		if err != nil {
			log.Fatal(err)
			return nil, err
		}
		foundParticipants = append(foundParticipants, participant)
	}
	return foundParticipants, nil
}

func (r *repo) ListenBySessionID(sessionID string, out chan []*Participant) error {
	fmt.Printf("Participant.ListenBySessionID: %s\n", sessionID)
	s := &participantSubscriber{}
	s.Start()
	r.subscribers.Push(s)
	go func() {
		participants, err := r.FindBySessionID(sessionID)
		if err != nil {
			fmt.Println(err)
		}
		out <- participants
		for {
			changedSessionID := <-s.changes
			if changedSessionID == sessionID {
				participants, err := r.FindBySessionID(sessionID)
				if err != nil {
					fmt.Println(err)
				}
				out <- participants
			}
		}
	}()
	return nil
}

func (r *repo) DeleteByID(ID string) error {
	_, err := r.db.Exec(`
		DELETE FROM participants
		WHERE id = $1
	`, ID)
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}
