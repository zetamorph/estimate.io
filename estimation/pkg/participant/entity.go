package participant

import (
	proto "github.com/zetamorph/estimate-io/proto"
)

// Participant entity
type Participant struct {
	ID        string `db:"id"`
	Name      string `db:"name"`
	SessionID string `db:"session_id"`
	IsManager bool   `db:"is_manager"`
}

// ToProto converts a Participant struct to a go-grpc struct
func (p *Participant) ToProto() *proto.Participant {
	return &proto.Participant{
		Name:      p.Name,
		Id:        p.ID,
		SessionId: p.SessionID,
		IsManager: p.IsManager,
	}
}
