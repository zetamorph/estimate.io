module github.com/zetamorph/estimate-io/estimation/session

go 1.12

require (
	github.com/danielvladco/go-proto-gql v0.2.0 // indirect
	github.com/gogo/protobuf v1.2.1
	github.com/jmoiron/sqlx v1.2.0
	github.com/zetamorph/estimate-io/estimation/util v0.0.0
	github.com/zetamorph/estimate-io/proto v0.0.0
)

replace github.com/zetamorph/estimate-io/estimation/util => ../util

replace github.com/zetamorph/estimate-io/proto => ../../proto
