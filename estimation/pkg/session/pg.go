package session

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/jmoiron/sqlx"
)

type repo struct {
	db *sqlx.DB
}

// NewPgRepository returns a new Postgres repository
func NewPgRepository(db *sqlx.DB) Repository {
	return &repo{
		db: db,
	}
}

func (r *repo) Create(session *Session) (*Session, error) {
	stmt, err := r.db.PrepareNamed(`
		INSERT INTO sessions (name, task_timebox, timebox)
		VALUES (:name, :timebox, :task_timebox)
		RETURNING *
	`)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	defer stmt.Close()
	var created Session
	err = stmt.Get(&created, session)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	return &created, nil
}

func (r *repo) FindByID(id string) (*Session, error) {
	var foundSession Session
	row := r.db.QueryRow(`
		SELECT id, name
		FROM sessions
		WHERE id = $1
	`, id)
	err := row.Scan(&foundSession.ID, &foundSession.Name)
	switch err {
	case sql.ErrNoRows:
		return nil, err
	case nil:
		return &foundSession, nil
	default:
		log.Fatal(err)
		return nil, err
	}
}
