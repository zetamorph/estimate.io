package session

// Repository ...
type Repository interface {
	Create(session *Session) (*Session, error)
	FindByID(id string) (*Session, error)
}
