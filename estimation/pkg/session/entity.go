package session

import (
	"database/sql"
	"fmt"
	"time"

	types "github.com/gogo/protobuf/types"
	util "github.com/zetamorph/estimate-io/estimation/util"
	proto "github.com/zetamorph/estimate-io/proto"
)

// ToProto converts a Session struct to a go-grpc struct
func (s *Session) ToProto() (*proto.Session, error) {
	createdAt, err := types.TimestampProto(s.CreatedAt)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	return &proto.Session{
		Name:        s.Name,
		Id:          s.ID,
		Timebox:     util.NullInt64ToProtoInt64(&s.Timebox),
		TaskTimebox: util.NullInt64ToProtoInt64(&s.TaskTimebox),
		CreatedAt:   createdAt,
	}, nil
}

// Session is an estimation session
type Session struct {
	ID          string        `db:"id"`
	Name        string        `db:"name"`
	TaskTimebox sql.NullInt64 `db:"task_timebox"`
	Timebox     sql.NullInt64 `db:"timebox"`
	CreatedAt   time.Time     `db:"created_at"`
}
