package estimate

// Repository for Estimate
type Repository interface {
	Create(estimate *Estimate) (*Estimate, error)
	FindByTaskID(taskID string) ([]*Estimate, error)
	ListenByTaskID(taskID string, estimateChanges chan []*Estimate) error
	CountByTaskID(taskID string) (*int32, error)
	DeleteByTaskID(taskID string) error
}
