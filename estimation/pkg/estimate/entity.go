package estimate

import (
	proto "github.com/zetamorph/estimate-io/proto"
)

// Estimate entity
type Estimate struct {
	ID            string `db:"id"`
	Estimate      int32  `db:"estimate"`
	TaskID        string `db:"task_id"`
	ParticipantID string `db:"participant_id"`
}

// ToProto converts an Estimate struct to a go-grpc struct
func (e *Estimate) ToProto() *proto.Estimate {
	return &proto.Estimate{
		Id:            e.ID,
		TaskId:        e.TaskID,
		Estimate:      e.Estimate,
		ParticipantId: e.ParticipantID,
	}
}
