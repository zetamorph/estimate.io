package estimate

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	"github.com/jmoiron/sqlx"

	bc "github.com/zetamorph/estimate-io/estimation/pgbroadcaster"

	pq "github.com/lib/pq"
)

type repo struct {
	db          *sqlx.DB
	broadcaster *bc.PgBroadcaster
}

// NewPgRepository - creates a new Postgres repo
func NewPgRepository(db *sqlx.DB, connStr string) Repository {
	reportProblem := func(ev pq.ListenerEventType, err error) {
		if err != nil {
			fmt.Println(err.Error())
		}
	}

	listener := pq.NewListener(connStr, 10*time.Second, time.Minute, reportProblem)
	err := listener.Listen("estimates_change")
	if err != nil {
		panic(err)
	}

	broadcaster := bc.NewPgBroadcaster()
	broadcaster.Init(listener, "TaskID")

	return &repo{
		db,
		broadcaster,
	}
}

// CountByTaskID Estimations
func (r *repo) CountByTaskID(taskID string) (*int32, error) {
	row := r.db.QueryRow(`
		SELECT COUNT(id)
		FROM estimates
		WHERE task_id = $1
	`, taskID)
	var count *int32
	err := row.Scan(&count)
	switch err {
	case sql.ErrNoRows:
		return nil, err
	case nil:
		return count, nil
	default:
		log.Fatal(err)
		return nil, err
	}
}

// Create an Estimate
func (r *repo) Create(estimate *Estimate) (*Estimate, error) {
	rows, err := r.db.Query(`
		INSERT INTO estimates (task_id, participant_id, estimate)
		VALUES ($1, $2, $3)
		RETURNING id, task_id, participant_id, estimate
	`, estimate.TaskID, estimate.ParticipantID, estimate.Estimate)
	defer rows.Close()
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	var createdEstimate Estimate
	for rows.Next() {
		if err := rows.Scan(&createdEstimate.ID, &createdEstimate.TaskID, &createdEstimate.ParticipantID, &createdEstimate.Estimate); err != nil {
			fmt.Println(err)
			return nil, err
		}
	}
	return &createdEstimate, nil
}

func (r *repo) ListenByTaskID(taskID string, estimateChanges chan []*Estimate) error {
	s := bc.NewSubscriber()
	r.broadcaster.AddSubscriber(s)
	go func() {
		initialEstimates, err := r.FindByTaskID(taskID)
		if err != nil {
			return
		}
		estimateChanges <- initialEstimates
		for {
			changedTaskID := <-s.Changes
			if changedTaskID == taskID {
				estimates, err := r.FindByTaskID(taskID)
				if err != nil {
					return
				}
				estimateChanges <- estimates
			}
		}
	}()
	return nil
}

func (r *repo) FindByTaskID(taskID string) ([]*Estimate, error) {
	var foundEstimates []*Estimate
	rows, err := r.db.Query(`
		SELECT id, task_id, participant_id, estimate
		FROM estimates
		WHERE task_id = $1
	`, taskID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		estimate := &Estimate{}
		if err := rows.Scan(&estimate.ID, &estimate.TaskID, &estimate.ParticipantID, &estimate.Estimate); err != nil {
			return nil, err
		}
		foundEstimates = append(foundEstimates, estimate)
	}
	return foundEstimates, nil
}

func (r *repo) DeleteByTaskID(taskID string) error {
	_, err := r.db.Exec(`
		DELETE FROM estimates
		WHERE task_id = $1
	`, taskID)
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}
