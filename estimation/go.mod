module github.com/zetamorph/estimate-io/estimation

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/Unknwon/bra v0.0.0-20181014071252-e3d443382332 // indirect
	github.com/Unknwon/com v0.0.0-20190214221849-2d12a219ccaf // indirect
	github.com/Unknwon/log v0.0.0-20150304194804-e617c87089d3 // indirect
	github.com/go-pg/pg v7.1.7+incompatible // indirect
	github.com/gogo/protobuf v1.2.1
	github.com/golang/protobuf v1.2.1-0.20190205222052-c823c79ea157
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.0.0
	github.com/micro/go-micro v0.26.1
	github.com/spf13/viper v1.3.1
	github.com/zetamorph/estimate-io/estimation/estimate v0.0.0
	github.com/zetamorph/estimate-io/estimation/participant v0.0.0
	github.com/zetamorph/estimate-io/estimation/session v0.0.0
	github.com/zetamorph/estimate-io/estimation/task v0.0.0
	github.com/zetamorph/estimate-io/estimation/util v0.0.0
	github.com/zetamorph/estimate-io/proto v0.0.0
	gopkg.in/fsnotify/fsnotify.v1 v1.4.7 // indirect
)

replace github.com/zetamorph/estimate-io/proto => ./proto

replace github.com/zetamorph/estimate-io/estimation/session => ./pkg/session

replace github.com/zetamorph/estimate-io/estimation/task => ./pkg/task

replace github.com/zetamorph/estimate-io/estimation/estimate => ./pkg/estimate

replace github.com/zetamorph/estimate-io/estimation/participant => ./pkg/participant

replace github.com/zetamorph/estimate-io/estimation/util => ./pkg/util

replace github.com/zetamorph/estimate-io/estimation/pgbroadcaster => ./pkg/pgbroadcaster
