package main

import (
	"database/sql"
	"flag"
	"fmt"
	io "io/ioutil"
	"os"
	"path/filepath"

	_ "github.com/lib/pq"
	"github.com/spf13/viper"
)

// Run - run the migrations
func run(db *sql.DB) {
	fmt.Println("Running migrations")
	ensureMigrationsTableExists(db)
	files, err := filepath.Glob("*.sql")
	handleError(err)
	if len(files) == 0 {
		handleError(fmt.Errorf("No migration files found"))
	}
	for _, filename := range files {
		if hasRun := migrationHasRun(db, filename); hasRun {
			continue
		}
		runMigration(db, filename)
		fmt.Printf("Applied migration: %s\n", filename)
	}
	fmt.Println("Successfully updated the schema")
}

func runMigration(db *sql.DB, filename string) {
	bytes, err := io.ReadFile(filename)
	handleError(err)
	queries := string(bytes)
	_, err = db.Exec(queries)
	handleError(err)
	_, err = db.Exec(`
		INSERT INTO migrations (name)
		VALUES ($1)
	`, filename)
	handleError(err)
}

func ensureMigrationsTableExists(db *sql.DB) {
	_, err := db.Exec(`
		CREATE TABLE IF NOT EXISTS migrations (
			name VARCHAR(255) NOT NULL UNIQUE
		)
	`)
	handleError(err)
}

func migrationHasRun(db *sql.DB, filename string) bool {
	var count uint8
	row := db.QueryRow(`
		SELECT COUNT(*)
		FROM migrations
		WHERE name = $1
	`, filename)
	err := row.Scan(&count)
	handleError(err)
	return count > 0
}

// Init sets defaults for environment variables
func Init() {
	viper.SetDefault("DB_HOST", "localhost")
	viper.SetDefault("DB_PORT", "5432")
	viper.SetDefault("DB_PASSWORD", "123456")
	viper.SetDefault("DB_NAME", "estimations")
	viper.SetDefault("DB_USER", "postgres")
	viper.AutomaticEnv()
}

// GetDbConnStr returns a sql connection string
func GetDbConnStr(dbName string) string {
	return fmt.Sprintf(
		"user=%s password=%s port=%s dbname=%s host=%s sslmode=disable",
		viper.Get("DB_USER"),
		viper.Get("DB_PASSWORD"),
		viper.Get("DB_PORT"),
		dbName,
		viper.Get("DB_HOST"),
	)
}

func main() {
	Init()
	dbConnStr := GetDbConnStr(viper.GetString("DB_NAME"))
	var clean bool
	flag.BoolVar(&clean, "clean", false, "Set this flag to clean the database before applying the schema changes")
	flag.Parse()
	if clean {
		cleanDB()
	}
	db, err := sql.Open("postgres", dbConnStr)
	handleError(err)
	defer db.Close()
	run(db)
}

func cleanDB() {
	// open a connection to another db to be able to drop the estimations db
	dbConnStr := GetDbConnStr("template1")
	db, err := sql.Open("postgres", dbConnStr)
	handleError(err)
	defer db.Close()
	// terminate current connections
	_, err = db.Exec(`
		SELECT pid, pg_terminate_backend(pid) 
		FROM pg_stat_activity 
		WHERE datname = 'estimations' AND pid <> pg_backend_pid();
	`)
	handleError(err)
	_, err = db.Exec(fmt.Sprintf("DROP DATABASE IF EXISTS %s", "estimations"))
	handleError(err)
	_, err = db.Exec(fmt.Sprintf("CREATE DATABASE %s", "estimations"))
	handleError(err)
	fmt.Println("Successfully cleaned the database")
}

func handleError(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
