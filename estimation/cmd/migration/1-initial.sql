CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE OR REPLACE FUNCTION notify_change() RETURNS TRIGGER AS $$
    DECLARE
        data json;
        notification json;
    BEGIN
        -- Convert the old or new row to JSON, based on the kind of action.
        -- Action = DELETE?             -> OLD row
        -- Action = INSERT or UPDATE?   -> NEW row
        IF (TG_OP = 'DELETE') THEN
            data = row_to_json(OLD);
        ELSE
            data = row_to_json(NEW);
        END IF;
        
        -- Contruct the notification as a JSON string.
        notification = json_build_object(
            'action', TG_OP,
            'data', data
        );
        -- Execute pg_notify(channel, notification)
        PERFORM pg_notify(TG_ARGV[0], notification::text);
        -- Result is ignored since this is an AFTER trigger
        RETURN NULL; 
    END;
$$ LANGUAGE plpgsql;

CREATE TABLE IF NOT EXISTS sessions (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    name VARCHAR(255) NOT NULL,
    timebox INT,
    task_timebox INT,
    created_at timestamptz DEFAULT NOW()
);

CREATE TYPE task_state AS ENUM ('active', 'inactive', 'complete');
CREATE TABLE IF NOT EXISTS tasks (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    name VARCHAR(255) NOT NULL,
    session_id UUID NOT NULL REFERENCES sessions(id) ON DELETE CASCADE,
    created_at timestamptz DEFAULT NOW(),
    state task_state DEFAULT 'inactive',
    estimation INT,
    estimates_visible BOOLEAN DEFAULT false,
    activated_at timestamptz
);

CREATE TRIGGER tasks_notify_change
AFTER INSERT OR UPDATE OR DELETE ON tasks
FOR EACH ROW EXECUTE PROCEDURE notify_change('tasks_change');

CREATE TABLE IF NOT EXISTS participants (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    name VARCHAR(255) NOT NULL,
    session_id UUID NOT NULL REFERENCES sessions(id) ON DELETE CASCADE,
    is_manager BOOLEAN DEFAULT false,
    created_at timestamptz DEFAULT NOW()
);

CREATE TRIGGER participants_notify_change
AFTER INSERT OR UPDATE OR DELETE ON participants
FOR EACH ROW EXECUTE PROCEDURE notify_change('participants_change');

CREATE TABLE IF NOT EXISTS estimates (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    task_id UUID NOT NULL REFERENCES tasks(id) ON DELETE CASCADE,
    estimate INT NOT NULL,
    participant_id UUID NOT NULL REFERENCES participants(id) ON DELETE CASCADE,
    created_at timestamptz DEFAULT NOW()
);

CREATE TRIGGER estimates_notify_change
AFTER INSERT OR UPDATE OR DELETE ON estimates
FOR EACH ROW EXECUTE PROCEDURE notify_change('estimates_change');