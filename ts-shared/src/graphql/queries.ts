import gql from 'graphql-tag';

const GET_SESSION = gql`
    query EstimationServiceGetSession ($in: GetSessionRequestInput) {
        estimationServiceGetSession(in: $in) {
            session {
                id
                name
                timebox { value }
                taskTimebox { value }
                createdAt { seconds }
            }
        }
    }
`;

const GET_CURRENT_SESSION = gql`
    query GetCurrentSession {
        currentSession @client {
            name
            id
            timebox { value }
            taskTimebox { value }
            createdAt { seconds }
        }
    }
`;

const GET_CURRENT_PARTICIPANT = gql`
    query GetCurrentParticipant {
        currentParticipant @client {
            name
            id
            isManager
        }
    }
`;

const GET_ESTIMATES = gql`
    query getEstimates($taskId: String!) {
        estimationServiceGetEstimates(in: {taskId: $taskId}) {
            estimates {id, participantId, taskId, estimate}
        }
    }
`;

export const QUERIES = {
    GET_CURRENT_PARTICIPANT,
    GET_CURRENT_SESSION,
    GET_ESTIMATES,
    GET_SESSION,
};
