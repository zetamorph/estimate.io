import gql from 'graphql-tag';

const CREATE_SESSION = gql`
    mutation EstimationServiceCreateSession (
        $sessionName: String!,
        $managerName: String!,
        $timebox: Int64ValueInput!,
        $taskTimebox: Int64ValueInput!,
    ) {
        estimationServiceCreateSession(in: {
            name: $sessionName,
            managerName: $managerName,
            timebox: $timebox,
            taskTimebox: $taskTimebox,
        }) {
            session {
                id
                name
                timebox { value }
                taskTimebox { value }
                createdAt { seconds }
            }
            participant {id, name, isManager, sessionId}
        }
    }
`;

const JOIN_SESSION = gql`
    mutation estimationServiceJoinSession ($sessionId: String, $participantName: String) {
        estimationServiceJoinSession(in: {sessionId: $sessionId, participantName: $participantName}) {
            participant {
                id
                name
                sessionId
                isManager
            },
            session {
                id
                name
                timebox { value }
                taskTimebox { value }
                createdAt { seconds }
            }
        }
    }
`;

const ADD_TASK = gql`
    mutation EstimationServiceAddTask ($name: String!, $sessionId: String!) {
        estimationServiceAddTask(in: {name: $name, sessionId: $sessionId}) {
            task { id, name, sessionId, state }
        }
    }
`;

const ACTIVATE_TASK = gql`
    mutation activateTask($taskId: String!) {
        estimationServiceActivateTask(in: { taskId: $taskId })
    }
`;

const DELETE_TASK = gql`
    mutation deleteTask($taskId: String!) {
        estimationServiceDeleteTask(in: { taskId: $taskId })
    }
`;

const COMPLETE_TASK = gql`
    mutation completeTask($taskId: String, $estimation: Int) {
        estimationServiceCompleteTask(in: {
            taskId: $taskId,
            estimation: $estimation,
        })
    }
`;

const RESET_TASK = gql`
    mutation resetTask($taskId: String!) {
        estimationServiceResetTask(in: { taskId: $taskId })
    }
`;

const REVEAL_ESTIMATES = gql`
    mutation revealEstimates($taskId: String!) {
        estimationServiceRevealEstimates(in: { taskId: $taskId })
    }
`;

const ADD_ESTIMATE = gql`
    mutation addEstimate($taskId: String!, $participantId: String!, $value: Int!) {
        estimationServiceAddEstimate(in: { estimate: {
            taskId: $taskId,
            participantId: $participantId,
            estimate: $value
        } }) {
            estimate {
                id
                taskId
                estimate
                participantId
            }
        }
    }
`;

const LEAVE_SESSION = gql`
    mutation leaveSession($sessionId: String!, $participantId: String!) {
        estimationServiceLeaveSession(in: { sessionId: $sessionId, participantId: $participantId })
    }
`;

export const MUTATIONS = {
    ACTIVATE_TASK,
    ADD_ESTIMATE,
    ADD_TASK,
    COMPLETE_TASK,
    CREATE_SESSION,
    DELETE_TASK,
    LEAVE_SESSION,
    JOIN_SESSION,
    RESET_TASK,
    REVEAL_ESTIMATES,
};
