import { DataProxy } from 'apollo-cache';
import { ExecutionResult } from 'graphql';
import { QUERIES } from '.';
import { Session, Participant } from '../models';

export interface ClientState {
    currentSession: Session | null;
    currentParticipant: Participant | null;
    clientId: string;
  }  

export const resolvers = {
    Mutation: {},
};

export const defaults = {
    currentParticipant: {
        __typename: 'CurrentParticipant',
        id: null,
        isManager: null,
        name: null,
    },
    currentSession: {
        __typename: 'CurrentSession',
        createdAt: {
            __typename: 'TimeStamp',
            seconds: null,
        },
        id: null,
        name: null,
        taskTimebox: {
            __typename: 'Int64Value',
            value: null,
        },
        timebox: {
            __typename: 'Int64Value',
            value: null,
        },
    },
};

export const UPDATE_CURRENT_PARTICIPANT_AND_SESSION = (operationName: string) =>
    (proxy: DataProxy, { data }: ExecutionResult) => {
        const participantQuery = QUERIES.GET_CURRENT_PARTICIPANT;
        const currentParticipant = proxy.readQuery({ query: participantQuery });
        proxy.writeQuery({
            data: {
                currentParticipant: {
                    ...currentParticipant,
                    ...data![operationName].participant,
                    __typename: 'CurrentParticipant',
                },
            },
            query: participantQuery,
        });

        const sessionQuery = QUERIES.GET_CURRENT_SESSION;
        const currentSession = proxy.readQuery({ query: sessionQuery });

        proxy.writeQuery({
            data: {
                currentSession: {
                    ...currentSession,
                    ...data![operationName].session,
                    __typename: 'CurrentSession',
                },
            },
            query: sessionQuery,
        });
    };
