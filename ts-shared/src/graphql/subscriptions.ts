import gql from 'graphql-tag';

const GET_PARTICIPANTS = gql`
  subscription listenParticipants ($sessionId: String) {
    estimationServiceListenParticipants(in: {sessionId: $sessionId}) {
      participants {id, name, isManager, sessionId}
    }
  }
`;

const GET_ESTIMATES = gql`
    subscription listenEstimates($taskId: String!) {
        estimationServiceListenEstimates(in: {taskId: $taskId}) {
            estimates {id, participantId, taskId, estimate}
        }
    }
`;

const GET_TASKS = gql`
    subscription listenTasks ($sessionId: String!) {
        estimationServiceListenTasks(in: {sessionId: $sessionId}) {
            tasks { id, name, sessionId, state, estimation, estimatesVisible }
        }
    }
`;

export const SUBSCRIPTIONS = {
    GET_ESTIMATES,
    GET_PARTICIPANTS,
    GET_TASKS,
};
