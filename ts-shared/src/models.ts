export interface Int64Value {
  value: number | null;
}

export interface TimeStamp {
  seconds: number;
}

export interface Session {
  id: string;
  name: string;
  timebox: Int64Value;
  taskTimebox: Int64Value;
  createdAt: TimeStamp;
}

export interface Task {
  id: string;
  state: TaskState;
  sessionId: string;
  name: string;
  estimation: number | null;
  estimatesVisible: boolean;
}

export interface Participant {
  id: string;
  name: string;
  isManager: boolean;
}

export enum TaskState {
  INACTIVE = 'INACTIVE',
  COMPLETE = 'COMPLETE',
  ACTIVE = 'ACTIVE',
}

export interface Estimation {
  id: string;
  taskId: string;
  participantId: string;
  estimate: Estimate;
}

export enum Estimate {
  ONE = 1,
  TWO = 2,
  THREE = 3,
  FIVE = 5,
  EIGHT = 8,
  THIRTEEN = 13,
  TWENTY = 20,
  FORTY = 40,
  HUNDRED = 100,
  UNKNOWN = -1,
  BREAK = -2,
}
