
import React from 'react';
import { adopt } from 'react-adopt';
import { Query, Subscription } from 'react-apollo';
import { Task, SUBSCRIPTIONS, QUERIES, Estimation, Participant, TaskState } from '..';
// tslint:disable:no-shadowed-variable

export interface SubscriptionsWithParticipantProps {
    render: any;
    taskId: string;
    sessionId: string;
}

const participantsSubscription = ({ sessionId, render }: SubscriptionsWithParticipantProps) => (
    <Subscription
        subscription={SUBSCRIPTIONS.GET_PARTICIPANTS}
        variables={{ sessionId }}
    >{render}</Subscription>
);

const estimatesSubscription = ({ render, taskId }: SubscriptionsWithParticipantProps) => (
    <Subscription
        subscription={SUBSCRIPTIONS.GET_ESTIMATES}
        variables={{ taskId }}
    >{render}</Subscription>
);

const currentParticipantQuery = ({ render }: SubscriptionsWithParticipantProps) => (
    <Query query={QUERIES.GET_CURRENT_PARTICIPANT}>{render}</Query>
);

const SubscriptionsWithParticipant = adopt({
    currentParticipantQuery,
    estimatesSubscription,
    participantsSubscription,
});

export interface EstimationListProps {
    estimatesVisible: boolean;
    hasVoted: boolean;
    taskIsComplete: boolean;
    participantId: string;
    estimations: Estimation[];
}

interface Props {
    task: Task;
    loading: JSX.Element;
    children: (props: EstimationListProps) => JSX.Element | JSX.Element[];
}

export const EstimationListContainer = ({ children, task, loading }: Props) => (
    <SubscriptionsWithParticipant taskId={task.id}>{({
        currentParticipantQuery,
        estimatesSubscription,
        participantsSubscription,
    }: any) => {
        if (
            estimatesSubscription.loading ||
            participantsSubscription.loading ||
            currentParticipantQuery.loading
        ) {
            return loading;
        }

        const currentParticipant: Participant = currentParticipantQuery.data.currentParticipant;
        const estimations: Estimation[] = estimatesSubscription.data.estimationServiceListenEstimates.estimates;
        const hasVoted = !!estimations.find((e) => e.participantId === currentParticipant.id);
        const taskIsComplete = task.state === TaskState.COMPLETE;
        const { estimatesVisible } = task;
        return children({
            estimatesVisible,
            hasVoted,
            taskIsComplete,
            participantId: currentParticipant.id,
            estimations,
        });
    }}</SubscriptionsWithParticipant>
);
