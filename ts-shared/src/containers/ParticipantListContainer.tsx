import React from 'react';
import { Subscription, Query, SubscriptionResult, QueryResult } from 'react-apollo';
import { adopt } from 'react-adopt';
import { SUBSCRIPTIONS, QUERIES } from '../graphql';
import { Participant } from '..';

interface RenderProps {
    sessionId: string;
    render?: any;
}

const participantsSubscription = ({ sessionId, render }: RenderProps) => (
    <Subscription
        subscription={SUBSCRIPTIONS.GET_PARTICIPANTS}
        variables={{ sessionId }}
    >{render}</Subscription>
);

const currentParticipantQuery = ({ render }: RenderProps) => (
    <Query query={QUERIES.GET_CURRENT_PARTICIPANT}>
        {render}
    </Query>
);

const SubscriptionWithParticipant = adopt<{
    query: QueryResult<{ currentParticipant: Participant }>,
    subscription: SubscriptionResult<{
        estimationServiceListenParticipants: {
            participants: Participant[],
        },
    }>,
}, RenderProps>({
    query: currentParticipantQuery,
    subscription: participantsSubscription,
});

interface Props {
    sessionId: string;
    loading: JSX.Element;
    children: (participants: Participant[], currentParticipant: Participant) => JSX.Element | JSX.Element[];
}

export const ParticipantListContainer = ({ sessionId, loading, children }: Props) => (
    <SubscriptionWithParticipant sessionId={sessionId}>{({ query, subscription }) => {
        if (query.loading || subscription.loading) {
            return loading;
        }
        const { participants } = subscription!.data!.estimationServiceListenParticipants;
        const { currentParticipant } = query!.data!;
        return <>{children(participants, currentParticipant)}</>;
    }}</SubscriptionWithParticipant>
);
