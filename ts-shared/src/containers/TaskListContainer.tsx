import React from 'react';
import { Subscription, Query } from 'react-apollo';
import { SUBSCRIPTIONS, QUERIES, Task } from '..';
import { adopt } from 'react-adopt';

interface RenderProps {
    render: any;
    sessionId: string;
}

const tasksSubscription = ({ render, sessionId }: RenderProps) => (
    <Subscription
        subscription={SUBSCRIPTIONS.GET_TASKS}
        variables={{ sessionId }}
    >{render}</Subscription>
);

const participantQuery = ({ render }: RenderProps) => (
    <Query query={QUERIES.GET_CURRENT_PARTICIPANT}>{render}</Query>
);

const TasksWithParticipant = adopt({
    query: participantQuery,
    subscription: tasksSubscription,
});

interface Props {
    sessionId: string;
    loading: JSX.Element;
    children: (tasks: Task[]) => JSX.Element | JSX.Element[];
}

export const TaskListContainer = ({ sessionId, loading, children }: Props) => (
    <TasksWithParticipant sessionId={sessionId}>{({ subscription, query }: any) => {
        if (subscription.loading || query.loading) {
            return loading;
        }
        const { tasks } = subscription.data.estimationServiceListenTasks;
        return <>{children(tasks)}</>;
    }}</TasksWithParticipant>
);
