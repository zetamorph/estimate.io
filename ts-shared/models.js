"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TaskState;
(function (TaskState) {
    TaskState["INACTIVE"] = "INACTIVE";
    TaskState["COMPLETE"] = "COMPLETE";
    TaskState["ACTIVE"] = "ACTIVE";
})(TaskState = exports.TaskState || (exports.TaskState = {}));
var Estimate;
(function (Estimate) {
    Estimate[Estimate["ONE"] = 1] = "ONE";
    Estimate[Estimate["TWO"] = 2] = "TWO";
    Estimate[Estimate["THREE"] = 3] = "THREE";
    Estimate[Estimate["FIVE"] = 5] = "FIVE";
    Estimate[Estimate["EIGHT"] = 8] = "EIGHT";
    Estimate[Estimate["THIRTEEN"] = 13] = "THIRTEEN";
    Estimate[Estimate["TWENTY"] = 20] = "TWENTY";
    Estimate[Estimate["FORTY"] = 40] = "FORTY";
    Estimate[Estimate["HUNDRED"] = 100] = "HUNDRED";
    Estimate[Estimate["UNKNOWN"] = -1] = "UNKNOWN";
    Estimate[Estimate["BREAK"] = -2] = "BREAK";
})(Estimate = exports.Estimate || (exports.Estimate = {}));
