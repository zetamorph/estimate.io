migrate-estimation:
	docker run \
		--network container:$(shell docker-compose ps -q db) \
		estimateio/estimation-migration:latest

#
# Estimation Service
#

build_estimation_service_image_dev:
	docker build \
		-t ${CI_REGISTRY_IMAGE}/estimation_service:dev \
		--cache-from=${CI_REGISTRY_IMAGE}/estimation_service:dev \
		--target dev \
		estimation

build_estimation_service_image_builder:
	docker build \
		-t ${CI_REGISTRY_IMAGE}/estimation_service:builder \
		--cache-from=${CI_REGISTRY_IMAGE}/estimation_service:dev,${CI_REGISTRY_IMAGE}/estimation_service:builder \
		--target builder \
		estimation

build_estimation_service_image: build_estimation_service_image_dev build_estimation_service_image_builder
	docker build \
		-t ${CI_REGISTRY_IMAGE}/estimation_service:${CI_COMMIT_SHA} \
		-t ${CI_REGISTRY_IMAGE}/estimation_service:latest \
		--cache-from=${CI_REGISTRY_IMAGE}/estimation_service:dev,${CI_REGISTRY_IMAGE}/estimation_service:builder \
		estimation

push_estimation_service_image_dev:
	docker push \
		${CI_REGISTRY_IMAGE}/estimation_service:dev

push_estimation_service_image_builder:
	docker push \
		${CI_REGISTRY_IMAGE}/estimation_service:builder

push_estimation_service_image: push_estimation_service_image_dev push_estimation_service_image_builder
	docker push ${CI_REGISTRY_IMAGE}/estimation_service:${CI_COMMIT_SHA}
	docker push	${CI_REGISTRY_IMAGE}/estimation_service:latest

#
# Estimation Service Migration Runner
#

build_estimation_service_migration_runner_image_builder:
	docker build \
		-t ${CI_REGISTRY_IMAGE}/estimation_service_migration_runner:builder \
		--cache-from=${CI_REGISTRY_IMAGE}/estimation_service_migration_runner:builder \
		--target builder \
		estimation/cmd/migration

build_estimation_service_migration_runner_image: build_estimation_service_migration_runner_image_builder
	docker build \
		-t ${CI_REGISTRY_IMAGE}/estimation_service_migration_runner:${CI_COMMIT_SHA} \
		-t ${CI_REGISTRY_IMAGE}/estimation_service_migration_runner:latest \
		--cache-from=${CI_REGISTRY_IMAGE}/estimation_service_migration_runner:builder \
		estimation/cmd/migration

push_estimation_service_migration_runner_image_builder:
	docker push \
		${CI_REGISTRY_IMAGE}/estimation_service_migration_runner:builder

push_estimation_service_migration_runner_image: push_estimation_service_migration_runner_image_builder
	docker push ${CI_REGISTRY_IMAGE}/estimation_service_migration_runner:${CI_COMMIT_SHA}
	docker push	${CI_REGISTRY_IMAGE}/estimation_service_migration_runner:latest

# 
# GraphQL API
#

build_graphql_api_image_dev:
	docker build \
		-t ${CI_REGISTRY_IMAGE}/graphql_api:dev \
		--cache-from=${CI_REGISTRY_IMAGE}/graphql_api:dev \
		--target dev \
		api

build_graphql_api_image_builder:
	docker build \
		-t ${CI_REGISTRY_IMAGE}/graphql_api:builder \
		--cache-from=${CI_REGISTRY_IMAGE}/graphql_api:dev,${CI_REGISTRY_IMAGE}/graphql_api:builder \
		--target builder \
		api

build_graphql_api_image: build_graphql_api_image_dev build_graphql_api_image_builder
	docker build \
		-t ${CI_REGISTRY_IMAGE}/graphql_api:${CI_COMMIT_SHA} \
		-t ${CI_REGISTRY_IMAGE}/graphql_api:latest \
		--cache-from=${CI_REGISTRY_IMAGE}/graphql_api:dev,${CI_REGISTRY_IMAGE}/graphql_api:builder \
		api

push_graphql_api_image_dev:
	docker push \
		${CI_REGISTRY_IMAGE}/graphql_api:dev

push_graphql_api_image_builder:
	docker push \
		${CI_REGISTRY_IMAGE}/graphql_api:builder

push_graphql_api_image: push_graphql_api_image_dev push_graphql_api_image_builder
	docker push ${CI_REGISTRY_IMAGE}/graphql_api:${CI_COMMIT_SHA}
	docker push	${CI_REGISTRY_IMAGE}/graphql_api:latest

#
# Web App
#

build_web_app_image_builder:
	docker build \
		-t ${CI_REGISTRY_IMAGE}/web_app:builder \
		--cache-from=${CI_REGISTRY_IMAGE}/web_app:builder \
		--target builder \
		--build-arg NPM_TOKEN=${NPM_TOKEN} \
		web

build_web_app_image: build_web_app_image_builder
	docker build \
		-t ${CI_REGISTRY_IMAGE}/web_app:${CI_COMMIT_SHA} \
		-t ${CI_REGISTRY_IMAGE}/web_app:latest \
		--cache-from=${CI_REGISTRY_IMAGE}/web_app:builder \
		--build-arg NPM_TOKEN=${NPM_TOKEN} \
		web

build_web_app_image_local:
	docker build \
		-t web_app \
		--build-arg NPM_TOKEN=${NPM_TOKEN} \
		web

push_web_app_image_builder:
	docker push \
		${CI_REGISTRY_IMAGE}/web_app:builder

push_web_app_image: push_web_app_image_builder
	docker push ${CI_REGISTRY_IMAGE}/web_app:${CI_COMMIT_SHA}
	docker push	${CI_REGISTRY_IMAGE}/web_app:latest

start-local:
	docker-compose \
		-f deployment/docker-compose.yml \
		-f deployment/docker-compose.local.yml \
		up